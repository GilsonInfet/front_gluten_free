import React, { useState } from 'react';
import { FaBeer, FaAdobe } from 'react-icons/fa';
import { Button, Carousel, Container, Spinner } from 'react-bootstrap'


function App() {

    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };

    const Carrossel = (
        <Carousel activeIndex={index} onSelect={handleSelect} pause={'hover'}>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://images.freeimages.com/images/small-previews/adf/sun-burst-1478549.jpg"
                    alt="First slide"
                />
                <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://i.postimg.cc/GtD71Gkj/questao-1.jpg"
                    alt="Second slide"
                />

                <Carousel.Caption>
                    <h3>Second slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src="https://i.postimg.cc/Jn6KXKQx/questao-2.jpg"
                    alt="Third slide"
                />

                <Carousel.Caption>
                    <h3>Third slide label</h3>
                    <p>
                        Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    )


    const [show, setShow] = useState(false);

    const escreva = () => {
        console.log("Função externa do modal")
        setShow(false)
    }

    return (
        <div>

            <MyModal
                show={show}
                title={"Título do modal"}
                funcaoFechar={() => setShow(false)}
                messagebody={"Bom dia. Este texto é do corpo do modal"}
                funcaoDesejada={() => escreva()}
            />


            <hr></hr>
            <br></br>


            <div className="carrouselBox">
                {/* {Carrossel} */}
            </div>
            <Container>            
            <h3> Lets go for a <FaBeer />? </h3>
            <FaAdobe></FaAdobe>


                <Button variant="primary" size="lg" block>
                    CONTAINER  #################### <FaBeer />
                </Button>



            <hr></hr>
            <br></br>



            <Button variant="primary" onClick={() => setShow(true)}>Primary</Button>{' '}




            <Button variant="secondary">Secondary</Button>{' '}
            <Button variant="success">Success</Button>{' '}
            <Button variant="warning">Warning</Button>{' '}
            <Button variant="danger">Danger</Button>
            <Button variant="info">Info</Button>{' '}
            <Button variant="light">Light</Button>
            <Button variant="dark">Dark</Button>{' '}
            <Button variant="link">Link</Button>

            <hr></hr>
            <br></br>

            <Spinner animation="grow" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>

            <hr></hr>
            <br></br>

            </Container>

        </div>
    );
}

export default App;
