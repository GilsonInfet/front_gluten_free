import React, {  useState } from 'react'
import { Container } from 'react-bootstrap'
import InfoForm from '../../components/info/infoForm'
import InfoHome from '../portal/home/info'

const InfoView = () => {

  
    return (
        <>
        <Container>
            <InfoForm  />
          
            <h6>Preview da Exibição de Lista de Informações</h6>
            <br/>
            <InfoHome showDelete/>
            </Container>
        </>
    )
}

export default InfoView



