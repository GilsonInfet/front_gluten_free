import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap'
import BannerForm from '../../components/banner/bannerForm'
import BannerList from '../../components/banner/bannerList'


/**No momento eu optei por não colocar um form de cadastrabanner
 * O banner hoje está exibindo uma lista de produtos highlighted
 * Da forma que o banner foi proposto em aula ele deve estar associado a um produto.
 * A lista de produtos pode ficar imensa. Entendo não ser necessário um form de banner 
 */
const BannerView = (props) => {

    const [isForm, setForm] = useState(false)
    const [update, setUpdate] = useState({})


    const updateBanner = (banner) => {
        setForm(true)
        setUpdate(banner)
    }
    useEffect(() => {
        const type = new URLSearchParams(props.location.search).get("view")
        setForm(type !== 'list') 

        return () => { };
    }, [props])

    return (
        <>
                        
                <BannerForm update={update} updateBanner={updateBanner} />
                
                <BannerList updateBanner={updateBanner} />
            
        </>
    )
}

export default BannerView
