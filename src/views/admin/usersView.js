import React, { useEffect, useState } from 'react'
import { Button, Container  } from 'react-bootstrap'
import { useLocation } from 'react-router-dom'
import UsersForm from '../../components/users/usersForm'
import UsersList from '../../components/users/usersList'
import { ContCol } from '../../util/lays'
import EquipDisplay from '../portal/about'

const UsersView = () => {

    const [isForm, setForm] = useState(true)
    const [update, setUpdate] = useState({})
    const location = useLocation();


       /** recebe o user a ser atualizado. Chama exibição do form de user*/
    const updateUser = (prd) => {

        setUpdate(prd)             
        setForm(true)
    }



    useEffect(() => {
        if (location.state?.update) {
            setForm(false)
        }


    }, [location])


     return (
        <>
<Container>
        <ContCol>
            <Button size="sm" variant="success" onClick={() => setForm(!isForm)}>
                {isForm ? "Lista de Usuários" : "Novo usuário"}
            </Button>
        </ContCol>


            <br/>
            <br/>
            { isForm
                ? <UsersForm update={update} />
                : <UsersList updateUser={updateUser} />
            }


            <hr />
            <h6>Preview da Exibição de colaboradores</h6>
            <br/>
            <EquipDisplay  />
            </Container>
        </>
    )
}

export default UsersView



