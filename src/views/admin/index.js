import React from 'react'
import Dash from './dash'
import { Route } from 'react-router-dom'
// import {Container} from 'react-bootstrap'
import styled from 'styled-components'
import LayoutAdmin from '../../components/Layout/admin/index.js'
import { FaTachometerAlt, FaPizzaSlice, FaImage, FaTools, FaTags } from 'react-icons/fa'

import Categorias from './categories'

const MenuItens = [
    {
        name: "DashBoard",
        path: '/',
        icon: <FaTachometerAlt />,
        component: () => <Dash />
    },
    {
        name: "Banner",
        path: '/banner',
        icon: <FaImage />,
        component: () => <Categorias/>
    },
    {
        name: "Categorias",
        path: '/categorias',
        icon: <FaTags />,
        component:  Categorias
    },
    {
        name: "Produtos",
        path: '/produtos',
        icon: <FaPizzaSlice />,
        component: () => <h1>Produtos</h1>
    },
    {
        name: "Serviços",
        path: '/servicos',
        icon: <FaTools />,
        component: () => <h1>Servicos</h1>
    }
]


//ESSA FOI A PRIMEIRA VERSÃO. COMO FOI DE CONFUSO ENTENDIMENTO ELE DEIXOU ESTE MÓDULO DE LADO E NÃO ESTÁ SENDO USADO.
//a MONTAGEM DESTA VISUALIUZAÇÃO ESTÁ AGORA EM src\routes.js BEM COMO A VISUALIZAÇÃO DAS PAGES DO PORTAL
//preenche os links de sidebar via props de layoutAdmin (layoutadmin possui a sidebar):
//Array MenuItens ao mesmo tempo monta as rotas de admin no route abaixo com arraymap,  
export default (props) => {

    return (
        <LayoutAdmin Menu={MenuItens}>
            {MenuItens.map((item, i) => (
                <Route key={i} exact basename={props.match.path} path={props.match.path + item.path} component={item.component} />
            ))}

            <Route exact basename={props.match.path} path={props.match.path + '/categorias'} component={Categorias} />
        </LayoutAdmin>
    )
}


 const Myh1 = styled.h1`
 color:white;
  background-color:"red";
 `
