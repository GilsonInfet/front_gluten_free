import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap'
import styled from 'styled-components'


import FormCategory from '../../components/category/form'
import ListCategory from '../../components/category/list'
import { ContCol } from '../../util/lays'

/**viewcategory view category viewcategoria*/
const Category = (props) => {

    
    const [isForm, setForm] = useState(false)
    const [update, setUpdate] = useState({})

    const updateCategory = (catg) => {
        setForm(true)
        setUpdate(catg)
    }
    useEffect(() => {
        const type = new URLSearchParams(props.location.search).get("view")
        setForm(type !== 'list') 

        return () => { };
    }, [props])

    return (
        <Categories>
<ContCol>
            <Button size="sm" variant="success" onClick={() => setForm(!isForm)}>
                {isForm ? "Lista" : "Novo"}
            </Button>
</ContCol>
            <hr />
            { isForm
                ? <FormCategory update={update} />
                : <ListCategory updateCategory={updateCategory} />
            }
 
        </Categories>
    )
}


export default Category;

const Categories = styled.div`
 
`

