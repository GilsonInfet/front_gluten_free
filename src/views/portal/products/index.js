import React, { useEffect, useState } from 'react'
import { Button, Container, Table, ButtonGroup, Row, DropdownButton, Dropdown } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'
import { productList } from '../../../store/products/products.actions'
import Swal from 'sweetalert2'
import { categoryList } from '../../../store/categories/category.actions'

//listagem de produtos do link produtos fora da tela de homeportal
export default () => {

    const products_redux = useSelector(state => state.product.products)
    const dispatch = useDispatch()
    const [prdFilter, setprdFilter] = useState("None")
    const categories_redux =
        useSelector(state => state.category.categories)

    useEffect(() => {
        dispatch(categoryList())
        dispatch(productList())

    }, [dispatch])


    const sortProductByName = products_redux.sort(function (a, b) {

        if (a.category > b.category) {
            return 1;
        }
        if (a.category < b.category) {
            return -1;
        }
        return 0;
    });

    const _detalhe = (prd) => {

        Swal.fire({

            title: prd.title,

            imageUrl: prd.photo,
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
            html:
                `Categoria : ${prd.category.name} <br/> ` +
                `Descrição : ${prd.description} <br/> ` +
                `Detalhes : ${prd.complete_description} <br/> ` +
                `Preço R$: ${prd.price.toFixed(2)} <br/> `
        })
    }


    const categoriesfiltred = sortProductByName.filter(
        (item) => {
            if (prdFilter === "None") {
                return item
            } else {
                return item.category.name === prdFilter
            }
        }
    )


    return (
        <Product>

            <TitlePage title="Produtos" sub={`Conheça nossa lista de produtos. ${prdFilter}`} />
            <br />
            <Container>
                
                    <Divt>
                        <div className="hidesmall">
                            <Row>
                                <FlexButtonGroup aria-label="Basic example">
                                    <Button onClick={() => setprdFilter("None")} variant="primary">Todos</Button>
                                    {categories_redux.map((catg, i) => (
                                        <Button onClick={() => setprdFilter(catg.name)} variant="secondary" >{catg.name}</Button>
                                    ))}
                                </FlexButtonGroup>
                            </Row>

                        </div>

                        <Row>
                            <div className="hidelarge">

                                <DropdownButton title="Dropdown" id="bg-nested-dropdown" >
                                    <Dropdown.Menu>
                                        <Dropdown.Item href="#/action-1" eventKey="1" onClick={() => setprdFilter("None")} variant="primary" > Todos </Dropdown.Item >
                                        {categories_redux.map((catg, i) => (
                                            <Dropdown.Item href={`#/action-${i}`} eventKey={i+1}  onClick={() => setprdFilter(catg.name)} variant="secondary"> {catg.name} </Dropdown.Item>
                                         ))}
                                    </Dropdown.Menu>
                                </DropdownButton>

                            </div>
                        </Row>
                    </Divt>

                    <br />
                    <Scrolable>
                    <Table striped hover>

                        <thead>
                            <tr>

                                <THeadItem>Nome do Produto</THeadItem>
                                <THeadItem>Categoria</THeadItem>
                                <THeadItem>Preço</THeadItem>
                                <THeadItem>Foto</THeadItem>
                            </tr>
                        </thead>
                        <tbody>
                            {categoriesfiltred.map((catg, i) => (
                                <tr key={i}>
                                    <TbodyItem>{catg.title}</TbodyItem>
                                    <TbodyItem>{catg.category.name}</TbodyItem>
                                    <TbodyItem>{"R$ " + catg.price.toFixed(2)}</TbodyItem>
                                    <TbodyItem>
                                        <div className="separe">
                                            <Responsive onClick={() => _detalhe(catg)} src={catg.photo} alt="First slide" />
                                        </div>
                                    </TbodyItem>
                                </tr>
                            ))}

                        </tbody>
                    </Table>
                </Scrolable>


            </Container>
        </Product>
    )
}


const Scrolable = styled.div`                    
    min-width: 100%;
    height: auto;
    overflow-x: auto;
    overflow-y: auto;    
    padding: 20px;
`

const Divt = styled(Container)`

    .hidelarge { 
        display : none; 
        width:100% !important;
        }

    @media screen and (max-width: 600px) {
        .hidesmall { display: none;  } 
        .hidelarge {display : block !important; 
                    justify-content:flex-end;
                    width:100% !important;
                    
                    
                   }

                   .dropdown-toggle,
.dropdown-menu {
  width: 100%;
}        
    }

`

const FlexButtonGroup = styled(ButtonGroup)`

display: flex;
flex: 1;

@media screen and (max-width: 600px) {
    display: block;
}

`

const Product = styled.div`
display:block;

color:black;


tbody{
background:#eee !important;

}

`


const Responsive = styled.img`
width: auto;
height: 60px;
margin:3px;

`

const THeadItem = styled.th`
    background: #666;
    color:#eee;
    text-align:center;
    
`

const TbodyItem = styled.td`
text-align:center;

    :nth-child(1){  width: 30%; }
    :nth-child(2){  width: 20%; }
    :nth-child(3){  width: 30%; }
    :nth-child(4){  width: 20%;
        .separe{
        cursor: pointer;
        display: flex;
        justify-content :space-around;
        }
    }
`