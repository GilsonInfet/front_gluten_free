import React from 'react'


import TitlePage from '../../../components/titlePage'
import styled from 'styled-components'
import ServicosHome from '../home/servicos'
import { Container } from 'react-bootstrap'
export default  (props) => {
    
  return (
    <Service>
        <TitlePage  title="Serviços" sub="Conheça nossa lista de serviços."/> 
        <Container>
        <ServicosHome showDelete={props.showDelete}></ServicosHome>
        </Container>
    </Service>
  )
}


const Service = styled.div`
display:block;
min-height:500px;

`

