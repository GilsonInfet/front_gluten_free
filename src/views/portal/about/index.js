import React, { useEffect } from 'react'
import { Row, Col, Container, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'

import { userList, userDrop } from '../../../store/users/users.actions'
import { swalConfirmation } from '../../../util'


//importado por src\views\portal\home\home.js
//exibido em detalhamento de sobre, na rota sobre: http://localhost:3000/sobre
export default (props) => {


    const users_redux = useSelector(state => state.user.users)
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(userList())
    }, [dispatch, users_redux])

const apagarUsuario = (userId) =>{

    swalConfirmation(`Vai mesmo deletar este usuário ${userId}?`, ()=>{
        dispatch(userDrop({USER: userId})) 
    })


}

    return (

        <Colabsdiv>

            <TitlePage title="Colaboadores" sub="Nossa Equipe." />
            <Container >
                <Row>

                    {users_redux.map((item, i) => (

                        <StyledCol sm md >
                            <img src={item.photo} alt={item.name} />
                            <br /> <br />
                            <h6>{item.name}</h6>
                            <hr />
                            <h6>{item.funcao}</h6>

                            <p>{item.briefing}</p>
                            {props.showdelete ? <Button onClick={()=>apagarUsuario(item._id)}>Não Exibir</Button> : ""}
                            {/* <br/><br/> */}
                            {/* {props.showdelete ? <Button onClick={()=>apagarUsuario(item._id)}></Button> : ""} */}
                        </StyledCol>
                    ))}

                </Row>
            </Container>

        </Colabsdiv>
    )
}

const Colabsdiv = styled.div`
    min-height: 400px;
    width: 100%;


`

const StyledCol = styled(Col)`
padding:5px;
    text-align: center;
    color: white;
    background-color: rgba(10,10,10,0.7);

    margin: 5px;

    
    img{

        min-width:30%;
        min-height:30%;

        max-width:40%;
        max-height:40%;
    }
`
