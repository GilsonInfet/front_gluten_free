
import React, { useEffect } from 'react'
import { Carousel, Col, Container, Row } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import { bannerList } from '../../../store/banner/banner.actions'



//TODO: o professor colocou outro modelo de banner customizado
/**BannerHome, vou colocar no carrousel os produtos que estão em highlight/destaque */
const BannerHome = () => {


    const dispatch = useDispatch()
    const bannerItens_redux = useSelector(state => state.banner.banners)

    useEffect(() => {

        dispatch(bannerList())

    }, [dispatch])


    /**PROPS: title , coment, photo*/
    function BannerType(props) {

        return (

            <Carousel.Item>
                <Responsive
                    className="d-block w-100"
                    src={props.photo}
                    alt="First slide"
                />
                <Carousel.Caption >
                    <h3>{props.title}</h3>
                    <p>{props.coment}</p>
                </Carousel.Caption>

            </Carousel.Item>

        )
    }



    return (
        <Container>
            <Row >
                <ColBgDark >
                    <div >
                        <Banner hidden={false} >
                            <Carousel>

                                {bannerItens_redux.map((item, i) => (

                                    <Carousel.Item>
                                        <Responsive className="d-block w-100" src={item.photo} alt="First slide" />
                                        <Carousel.Caption >
                                            <h3>{item.title}</h3>
                                            <p>{item.coment}</p>
                                        </Carousel.Caption>
                                    </Carousel.Item>
                                )
                                )}
                            </Carousel>
                        </Banner>

                    </div>
                </ColBgDark>
            </Row>
        </Container>
    )
}

export default BannerHome


const ColBgDark = styled(Col)`
    background-color: rgba(10,10,10,0.7);
`

const Responsive = styled.img`
    width: 100%;
    height: auto;
`

const Banner = styled.div`
    margin-top : 5px;
    margin-bottom : 40px;
    /* width:100%; */
    color: white;
    text-align:center;
    /* background-color: rgba(10,10,10,0.7); */
    h3{
        margin-top : 45px;
    }

    img {
        max-height : 500px;
    }

    .carousel-indicators li {
        height: 10px;
        
    }

    .carousel-indicators {
        bottom: -50px;
    }
`
