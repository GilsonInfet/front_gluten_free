
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { Row, Col  } from 'react-bootstrap'

import { getWhatIWant } from '../../../services/admin'

const AboutHome = () => {



    const [about, setAbout] = useState({
        photo: "",
        title: "",
        description: "",
        direction: ""
    })

    useEffect(() => {
        const get = async () => {
            const prd = await getWhatIWant('content');
            setAbout(prd.data.about)
        }
        get();
    }, [])


    const placeimage = (
        <>
            <Col sm>
                <img src={about.photo} alt={about.title} />
            </Col>
        </>
    )

    return (
        <About >
            <Row>
                {about.direction === "LEFT" ? placeimage : ""}
                <Col sm >
                    <div className="description">
                        <h1>{about.title}</h1>
                        <br></br>
                        <p>{about.description}</p>
                    </div>
                </Col>
                {about.direction === "RIGHT" ? placeimage : ""}
            </Row>
        </About>
    )
}

export default AboutHome


const About = styled.div`
    width: 100%;
    background-color: rgba(10,10,10,0.7);
    color: #fff;
    min-height: 300px;


    .description{
        padding: 20px;
        display:flex;
        flex-direction:column;
        justify-content: center;
    }
    img{
        padding:5px;
        max-height: 400px;
        height:300px;
        max-width: 100%;
    }
`
