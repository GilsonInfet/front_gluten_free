import React from 'react'
import styled from 'styled-components'
// import { FaBeer } from 'react-icons/fa';

import Banner from './banner'
import Info from './info'
import About from './about'
import Servicos from './servicos'
import Products from './products'
import { Container } from 'react-bootstrap'

/**View Home, componentes exibidos em Home Portal */
const Home = () => {
    return (
        <HomeContainer>
            <Banner />
            <Info />
            <Products />
            <About />
            <Servicos />
            
        </HomeContainer>
    )
}

export default Home

const HomeContainer = styled(Container)`
`
