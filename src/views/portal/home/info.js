
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { Row, Col, Button } from 'react-bootstrap'
import { deleteInfoItem, getWhatIWant } from '../../../services/admin'
import { message, swalConfirmation } from '../../../util'


const InfoHome = (props) => {

    const [reload, setReload] = useState(0)
    const [info2, setInfo2] = useState([])
    const [contentAtual, setcontentAtual] = useState("")


    //TODO get info to  home info
    useEffect(() => {
        const get = async () => {
            const prd = await getWhatIWant('content');
            setInfo2(prd.data.infos)
            setcontentAtual(prd.data._id)
        }
        get();
    }, [reload])



    const deleteEsteInfoItem = (itemToDelete) => {


        const data = { _id: itemToDelete }

        swalConfirmation(`Você realmente quer apagar este item de Informação?`, ()=>{
            
            deleteInfoItem(contentAtual, data)
                .then((res) => {
                    message('success', `Item Info excluído.`)
                    setReload(reload + 1)

                })
                .catch((err) => message('error', `Erro ao tentar eliminar o item.`))
        })

        // if (window.confirm(`Você realmente quer apagar item ${itemToDelete}  de ${contentAtual}?`)) {
        //     deleteInfoItem(contentAtual, data)
        //         .then((res) => {
        //             message('success', `Item Info excluído.`)
        //             setReload(reload + 1)

        //         })
        //         .catch((err) => message('error', `Erro ao tentar eliminar o item.`))
        // }

    }



    return (
       
            <>
            <br/>
                <Row xs={1} sm={1} md={3} lg={3} xl={3} >

                    {info2.map((item, i) => (
                        <Col  >
                            <ItemInfo>
                                {/* <div> */}
                                <br />
                                <img src={item.photo} height='100' width="100" alt="First slide" />
                                <br />
                                <h6>{item.text || "nots"}</h6>

                                {item.link ? <a href={item.link || "nots"}> Link. </a> : <br />}

                                {props.showDelete ? <Button onClick={() => deleteEsteInfoItem(item._id)}>{`Eliminar item ${item._id}`} </Button> : ""}

                                {/* </div> */}
                            </ItemInfo>
                        </Col>
                    ))}
                </Row>
            </>
        

    )
}

export default InfoHome


const ItemInfo = styled.div`
    background-color: rgba(10,10,10,0.7);
    color:honeydew;
    width: 100%;
    padding: 0px;
    display:flex;
    flex-direction:column;
    justify-content: center;
    align-items: center;
    margin-bottom:10px;
`
