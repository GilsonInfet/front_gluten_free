import React, { useEffect, useState } from 'react'
import { Container, Col, Row, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import ServiceImg from '../../../assets/images/pao1.png'
import { deleteServiceItem, getWhatIWant } from '../../../services/admin'
import { actionToggleUpadating } from '../../../store/adminmenu/adminmenu'
import { message, swalConfirmation } from '../../../util'

//front_gluten_free\src\assets\images\bolo.png
//Exibe os serviços cadastrados na tela Home
const ServicosHome = (props) => {

const dispatch = useDispatch()
// actionToggleUpadating
    const [serviceArr, setServiceArr] =  useState([
        { photo:ServiceImg,
        description:"description",
        order:0 }
    ])
    const [serviceTitle, setServiceTitle] =  useState("Temp serviceTitle")
    const [serviceDescrip, setServiceDescip] =  useState("Temp serviceDescrip")
    const [contentAtual, setContentAtual] =  useState("")
    const [refresh, setRefresh] =  useState(0)

const atualizou = useSelector(state=> state.admmenu.updating)

useEffect(() => {
    const get = async () => {
        const content = await  getWhatIWant('content');
        setServiceArr(content.data.services.service )
        setServiceTitle(content.data.services.title )
        setServiceDescip(content.data.services.description)
        setContentAtual(content.data._id)
    }
    get();
    // return () => get = () => { };
}, [props.refresh, refresh, atualizou])

const apagarItemService = (itemToDelete)=>{
    const data = { _id : itemToDelete }

    swalConfirmation(`Você realmente quer apagar item ${itemToDelete}  ?`, ()=>{
        deleteServiceItem(contentAtual, data) 
        .then(function (response){
            dispatch(actionToggleUpadating())
            message("sucess", "Item eliminado.")
        })
        setRefresh(refresh + 1)
    })

   
}
    return (
             <>       
        <Servicos>

                <h2 className="text-center">{serviceTitle}</h2>
                <h5 className="text-center">{serviceDescrip}</h5>
              <Container>
                <Row md={props.listmode || 0} >
                {serviceArr.map((item, i) => (
                    
                        <Item xm sm>
                            
                        <img src={item.photo} height='100' width="140"  alt={serviceDescrip}/>
                        <br/> 
                        <h6>{ item.description }</h6>  
                        <br/> 
                        <p>{ item.detail }</p> 
                       

                        {props.showDelete ? <Button onClick={()=> apagarItemService(item._id) }>Apagar Item</Button> : ""}
                        
                        </Item>
                        
                    
                    ))}
                </Row>
                </Container>
        </Servicos>  
         </>
    )
}

export default ServicosHome


const Servicos = styled.div`
justify-content:center;

    margin-top:10px;
    margin-bottom:10px;
    padding-bottom:10px;
    width: 100%;
    color: white;
    background-color: rgba(10,10,10,0.7);
    img{
        margin-top:5px;
    }
`

/** é uma coluna de bootstrap*/
const Item = styled(Col)`
/* padding:5px; */
    text-align: center;
    color: white;
    background-color: rgba(10,10,10,0.7);
    /* height: auto; */
    /* max-width: 30%; */
    margin: 5px;
`
