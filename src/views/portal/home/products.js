import React, { useEffect } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import { productList } from '../../../store/products/products.actions'

// src\store\products\products.actions.js
/**Aqui é o componente products exibido na home */
const ProductsHome = () => {

    //implemnetar redux para pegar a lista de produtos e exibir em home/produtos (final da tela)

    //state => state.product.products
    // a parte "state => state" é padrão da sintaxe
    // "product.products" producer é o reducer que a store importou e atribuiu-lhe este nome "product", 
    const products_redux = useSelector(state => state.product.products)
    const dispatch = useDispatch()


    useEffect(() => {
        dispatch(productList())

    }, [dispatch])


    const products_redux_filtred = products_redux.filter(
        (item) => {
            return item.highlight             
        }
    )



    return (
        <>




        <Products>
        <h5>Destaques</h5>

        <div className="tooltip">Hover over me
  <span className="tooltiptext">Tooltip text</span>
</div>

        </Products>
            <Products>
                
                <Row >
                    <Col sm={12}>
                        {products_redux_filtred.map((item, i) => (
                            
                                <Responsive src={item.photo} alt="First slide" />

                            
                        ))}
                         
                    </Col>
                </Row>

            </Products>
            <br/>

        </>
    )
}

export default ProductsHome

const Products = styled.div`
width:100%;
background-color: rgba(10,10,10,0.7);
/* margin-bottom:10px; */
color:white;
padding: 5px;
display:flex;
justify-content :center;
`
const Responsive = styled.img`
width: auto;
height: 200px;
margin:3px;

`


