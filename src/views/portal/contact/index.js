import React from 'react'
import TitlePage from '../../../components/titlePage'
import { Container } from 'react-bootstrap'
import Contato from './contato'
import styled from 'styled-components'

export default () => {
    return (
        <Service>
            <TitlePage title="Contato" sub="Mande uma mensagem." />
            <Container>
                <Contato />
            </Container>
        </Service>
    )
}

const Service = styled.div`
display:block;
min-height:500px;

`