import React, { useState } from 'react'
import emailjs from 'emailjs-com';
import { init } from 'emailjs-com';
import { Col, Row, Button, Form, Spinner } from 'react-bootstrap';

import { message as alerta } from '../../../../src/util/index'
// src\util\index.js
const Contato = () => {

    // Email validation
    const emailRegex = RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);
    init("user_UeDDUdLb8xg9AXQRmPBV5");
    const [loading, setLoading] = useState(false)


    const [state, setForm] = useState({
        name: '',
        email: '',
        subject: '',
        message: '',
        formErrors: {
            name: '',
            email: '',
            subject: '',
            message: ''
        }
    })


    // Form validation
    const formValid = ({ formErrors, ...rest }) => {
        let valid = true;

        // Validate form errors being empty
        Object.values(formErrors).forEach((val) => {
            val.length > 0 && (valid = false);
        });

        // Validate the form was filled out
        Object.values(rest).forEach((val) => {
            val === '' && (valid = false);
        });

        return valid;
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true)
        if (formValid(state)) {
            // Handle form validation success
            const { name, email, subject, message } = state;

            // Set template params
            let templateParams = {
                to_name: "Glute Free",
                from_name: name,
                name: name,
                email: email,
                from_email: email,
                reply_to: email,
                subject: subject,
                message: message,
            };
            //  'user_UeDDUdLb8xg9AXQRmPBV5'
            emailjs.send('emailumblergp', 'template_l9b1qoj', templateParams,  process.env.EMAILJS_USER)
             .then(function(response) {
                alerta('info', `Sua mensagem foi enviada sucesso.`);
             }, function(error) {
                alerta('info', `Houve um erro no envio da mensagem`);
             })
             .finally(function (){
                setLoading(false)
                resetForm();
             })
        }
    };

   /**Reseta o formulário */
    const resetForm = () => {
        setForm({
            name: '',
            email: '',
            subject: '',
            message: '',
            formErrors: {
                name: '',
                email: '',
                subject: '',
                message: ''
            }
        });
    }


    const handleChange = (e) => {
        e.preventDefault();
        const { name, value } = e.target;
        let formErrors = { ...state.formErrors };

        switch (name) {
            case 'name':
                formErrors.name = value.length < 1 ? 'Please enter your name.' : '';
                break;
            case 'email':
                formErrors.email = emailRegex.test(value) ? '' : 'Please enter a valid email address.';
                break;
            case 'subject':
                formErrors.subject = value.length < 1 ? 'Please enter a subject.' : '';
                break;
            case 'message':
                formErrors.message = value.length < 1 ? 'Please enter a message' : '';
                break;
            default:
                break;
        }
        setForm({
            ...state,
            formErrors: formErrors,
            [name]: value
        });
        // setFormErros({ ...formErrors, [name]: value });
    };

    return (

        <>

            <br />

            <Row>
                <Col className="bg" xs={12} md={6}>

                <Form.Control 
                            type='text'
                            name='name'
                            value={state.name}
                            // className={`form-control formInput`}
                            onChange={handleChange}
                            placeholder='Nome'
                            noValidate>
                </Form.Control>
                <br /> 
                <Form.Control
                            type='email'
                            name='email'
                            value={state.email}
                            // className={`form-control formInput`}
                            onChange={handleChange}
                            placeholder='Email'
                            noValidate
                        />
                <br />
                <Form.Control
                            type='text'
                            name='subject'
                            value={state.subject}
                            // className={`form-control formInput`}
                            onChange={handleChange}
                            placeholder='Assunto'
                            noValidate
                        ></Form.Control>
                <br />
                <Form.Control as="textarea"
                            rows={5}
                            name='message'
                            value={state.message}
                            className={`form-control formInput`}
                            onChange={handleChange}
                            placeholder='Mensagem'
                            noValidate>
                </Form.Control>
                <br />
                <Button onClick={handleSubmit} color="secondary" type='submit' disabled={!formValid(state)}> 
                    {loading ?   <Spinner animation="grow" role="status"/> : "Enviar"}
                 </Button>
                <br />
                <br />


                </Col>

            </Row>

        </ >
    )
}

// https://medium.com/weekly-webtips/simple-react-contact-form-without-back-end-9fa06eff52d9

export default Contato
