

import React from 'react'
import { Route } from 'react-router-dom'
import { Container } from 'react-bootstrap'

//estes imports estão dentro da src\views\portal
import Home from './home/home'
import About from './about/index'
import Product from './products'
import Service from './service'
import Contact from './contact'

import Layout from '../../../src/components/Layout/portal/index'
import { Sharegroup } from '../../util/shares'


//src\views\portal\index.js - indexador das Rotas de portal
export default (props) => {
    return (        
            <Layout >
                
                <Container>

                    <Route exact basename={props.match.path}  path={props.match.path + '/'} component={Home} />
                    <Route exact basename={props.match.path}  path={props.match.path + 'sobre'} component={About} />
                    <Route exact basename={props.match.path}  path={props.match.path + 'produtos'} component={Product} />
                    <Route exact basename={props.match.path}  path={props.match.path + 'servicos'} component={Service} />
                    <Route exact basename={props.match.path}  path={props.match.path + 'contato'} component={Contact} />
                </Container>
            </Layout >      
    )
}


