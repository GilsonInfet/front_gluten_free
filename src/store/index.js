import { applyMiddleware, combineReducers, createStore } from 'redux'
import Reactotron from '../plugins/ReactotronConfig'
import { composeWithDevTools } from 'redux-devtools-extension'
import categoryReducer from './categories/category.reducer'
import productReducer from './products/products.reducer'
import userReducer from './users/users.reducer'
import bannerReducer from './banner/banner.reducer' //ajustar conforme endereços
import {admMenuReducer} from './adminmenu/adminmenu'
import thunk from 'redux-thunk'
import multi from 'redux-multi'
import { reducerGuardaContent } from './content/contentsaver'

// modularizações dos reduces que vc importou (agrupando-os em um objeto)
const reducers = combineReducers({
    admmenu: admMenuReducer,
    category: categoryReducer,
    product: productReducer,
    banner: bannerReducer,
    content: reducerGuardaContent,
    user: userReducer
})

// middlewares de confifurações do projeto
const middleware = [thunk, multi]

// compose que junta os middlewares e ferramentas de debug
const compose = composeWithDevTools(
    applyMiddleware(...middleware),
    Reactotron.createEnhancer()
)

// criação da store
const store = createStore(reducers, compose)

export default store


