

//reducer.js
import {  CATEGORY_LIST,   CATEGORY_LOADING,
    CATEGORY_CREATE } from './category.actions'

const INITIAL_STATE = { categories: [], loading: false  } //declara os estados

const reducer = (state = INITIAL_STATE, action) => {     //recebe a action
    switch (action.type) {        //decide o que fazer dependendo da action
        case CATEGORY_LOADING:
            state.loading =   !state.loading
            return state
        case CATEGORY_LIST:   state.categories = action.data
            return state
        case CATEGORY_CREATE: state.categories.push(action.data)
            return state
        default:
            return state
    }
}
export default reducer


