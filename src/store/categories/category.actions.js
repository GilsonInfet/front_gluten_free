import { message } from "../../util";
import { createCategory, deleteCategory, getCategories } from "../../services/admin";
import history from "../../config/history";

export const CATEGORY_LIST = "CATEGORY_LIST"
export const CATEGORY_CREATE = "CATEGORY_CREATE"
export const CATEGORY_UPDATE = "CATEGORY_UPDATE"
export const CATEGORY_LOADING = "CATEGORY_LOADING"

export const categorySave = (props, id = null) => {
    return async (dispatch) => {
        if (id === null) {
            const { data } = await createCategory(props)
            dispatch({ type: CATEGORY_CREATE, data });
            history.push('/admin/categorias?view=list')
        } else {
            //TODO: 
            //1-dispara load
            //2-update
            //3-get list
            //4-dispara gategoryList
            //5-dispara load

            // dispatch({ type: CATEGORY_LOADING })
            // await updateCategory(id, props)
            // const { data } = await getCategories()
            // dispatch({ type: CATEGORY_LIST, data })
            // dispatch({ type: CATEGORY_LOADING })
        }
    };
};



/**
 x.action.js : É  QUEM DE FATO AGE, REALIZA O TRABALHO
 1º dispara o aviso de loading, 
 2º obtém a lista de fato
 3º dispara solicitação de guardar a lista
 4º pede para desligarem o loading */

 export const categoryList = props => {
    return async (dispatch) => {
        dispatch({ type: CATEGORY_LOADING })
        const { data } = await getCategories()
        dispatch({ type: CATEGORY_LIST, data })
        dispatch({ type: CATEGORY_LOADING })
    };
}

/**Deleta a categoria informada por meio de seu _id */
export const categoryDrop = props => {
    return async (dispatch) => {

        const { category } = props
        dispatch({ type: CATEGORY_LOADING })

        deleteCategory(category._id)
            .then(res => {
                if (res.status === 200) {
                    dispatch(categoryList())
                    dispatch({ type: CATEGORY_LOADING })
                    message('success', `Categoria excluída com sucesso.`)
                }
            }).catch(error => {
                message('error', ` ${error.response.data.error}`)
            })

    };
}
