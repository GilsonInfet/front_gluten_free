import { message } from "../../util";
import { createUser, deleteUser,  getWhatIWant } from "../../services/admin";
import history from "../../config/history";

export const USER_LIST = "USER_LIST"
export const USER_LIST_ALL = "USER_LIST_ALL"

export const USER_CREATE = "USER_CREATE"
export const USER_UPDATE = "USER_UPDATE"
export const USER_LOADING = "USER_LOADING"

export const userSave = (props, id = null) => {
    return async (dispatch) => {
        if (id === null) {
            const { data } = await createUser(props)
            dispatch({ type: USER_CREATE, data });
            history.push('/admin/categorias?view=list')

        } else {
            // const { data } = await updateUSER(id, props)
            // dispatch({ type: USER_UPDATE, { name: 1}, id });
        }
    };
};


/**Pede a atualização da lista de usuários 'show=true' em store */
export const userList = props => {
    return async (dispatch) => {
        dispatch({ type: USER_LOADING })
        const { data } = await getWhatIWant('user','show=true')
        dispatch({ type: USER_LIST, data })
        dispatch({ type: USER_LOADING })
    };
}



/**Pede a atualização da lista de usuários 'show=true' em store */
export const userListAll = props => {
    return async (dispatch) => {
        dispatch({ type: USER_LOADING })
        const { data } = await getWhatIWant('user')
        dispatch({ type: USER_LIST_ALL, data })
        dispatch({ type: USER_LOADING })
    };
}

export const userDrop = props => {
    return async (dispatch) => {

        dispatch({ type: USER_LOADING })

        deleteUser(props)
            .then(res => {
                if (res.status === 200) {
                    dispatch(userList())
                    dispatch({ type: USER_LOADING })
                    message('success', `Usuário excluído com sucesso.`)
                }
            }).catch(error => {
                message('error', ` ${error.response.data.error}`)
            })

    };
}
