

import {
    USER_LIST,
    USER_LOADING,
    USER_CREATE,
    USER_LIST_ALL
} from './users.actions'

const INITIAL_STATE = {
    usersAll: [],
    users: [],
    loading: false
}

const reducer = (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case USER_LOADING:
            state.loading = !state.loading
            return state
        case USER_LIST:
            state.users = action.data
            return state
        case USER_LIST_ALL:
            state.usersAll = action.data
            return state    
        case USER_CREATE:
            state.users.push(action.data)
            return state
        default:
            return state
    }

}

//importado por src\store\index.js
export default reducer
