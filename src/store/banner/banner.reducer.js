import { BANNER_LIST, BANNER_LOADING, BANNER_CREATE} from './banner.actions'
   
   const INITIAL_STATE = { banners: [], loading: false  } //declara os estados
   
   const reducer = (state = INITIAL_STATE, action) => {     

       switch (action.type) {
           case BANNER_LOADING:
               state.loading = !state.loading
               return state
           case BANNER_LIST:
                state.banners = action.data
               return state
           case BANNER_CREATE:
               state.banners.push(action.data)
               return state
           default:
               return state
       }
   }
   
   //será importado por store.js
   export default reducer