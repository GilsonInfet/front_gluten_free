import { message } from "../../util"; //Modal Mensagem. Ajustar ou remover conforme necessidade
import { create_banner, delete_banner, get_banner, update_banner } from "../../services/admin"; //ajuste o nome e entereço de tuas funções


export const BANNER_LIST = "BANNER_LIST"
export const BANNER_CREATE = "BANNER_CREATE"
export const BANNER_UPDATE = "BANNER_UPDATE"
export const BANNER_LOADING = "BANNER_LOADING"

//Esta função grava ( ou se atualiza id != null) teu registro no mongo
//será importado pelo componente e usado num dispatch. 
/**Cria um novo Banner. Se recebe um id atualiza este banner */
export const bannerSave = (content, data, config , id = null) => {
    
    return async (dispatch) => {
        if (id === null) {
            // debugger
            console.log(`Recebeu um banner p CREATE em bannersave ${id}`)
            dispatch({ type: BANNER_LOADING })
            const  dados  = await create_banner(content, data, config)
            // console.log( "await create_banner(content, data, config)",  dados.data.banner, data)
            const bannersAtualizados = dados.data.banner
            const index = bannersAtualizados.length - 1
            const inserir = bannersAtualizados[index]
            // console.log( "bannersAtualizados",  bannersAtualizados)
            console.log( "bannersAtualizados inserir",  inserir)

             dispatch({ type: BANNER_CREATE, data : inserir});

            dispatch({ type: BANNER_LOADING })
            // dispatch(bannerList())

        } else {
            console.log(`Recebeu um banner p UPDATE em bannersave ${id}`)
            dispatch({ type: BANNER_LOADING })
            await update_banner(content, id, data, config)
            // const { data } = await get_banner()
            dispatch({ type: BANNER_LOADING })
            dispatch(bannerList())
        }
    };
};

//faz getlist e manda para o reducer salvar
 export const bannerList = props => {
    return async (dispatch) => {
        dispatch({ type: BANNER_LOADING })

        const  data  = await get_banner()

        dispatch({ type: BANNER_LIST, data })
        dispatch({ type: BANNER_LOADING })
    };
}

/**Action que deleta o banner */
export const bannerDrop = (contentatual, bannerId) => {
    return async (dispatch) => {
        dispatch({ type: BANNER_LOADING })
        delete_banner(contentatual, bannerId)
            .then(res => {
                if (res.status === 200) {
                    dispatch(bannerList())
                    dispatch({ type: BANNER_LOADING })
                    message('success', 'Banner excluído com sucesso.')
                }
            }).catch(error => {
                message('error',  "$ {error.response.data.error} ")  //ajustar ou remover conforme necessidade
            })

    };
}