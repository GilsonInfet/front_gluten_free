//reducer e action


import { getWhatIWant, updateContent } from "../../services/admin"

const INITIAL_STATE = {
    content: "",
    bgHomeImage: "",
    aboutTitle: "",
    aboutDescription: "",
    objeto: {
        facebook: "",
        instagram: "",
        twitter: "",
        youtube: "",
    },
    contato: {
        address: "",
        email: "",
        phone: ""
    },
    business: {
        logo: "",
        title: ""
    }

}

/**Action que executa patch em content, atualizando as inofrmações no banco de dados */
export const actionAtualizaContent = (content, data, config ) => {
    
    return async (dispatch) => {
            await updateContent(content, data, config)
            dispatch(actionGuardaContent())
    };
};

/**Action que busca nº do content e foto do bground, e várias outras coisas do objeto content
 * Actiosn types: GUARDA_CONTENT,  GUARDA_BGIMAGE ... */
export const actionGuardaContent = props => {
    return async (dispatch) => {

        const content = await getWhatIWant('content')
        const { _id, photo } = content.data
        const aboutTitle = content.data.about.title
        const aboutDescription = content.data.about.description
        const { facebook, instagram, twitter, youtube } = content.data.social
        const { email, phone, address } = content.data.contato
        const { title } = content.data.business
        const logo = content.data.business.photo

        dispatch({ type: "GUARDA_CONTENT", _id })
        dispatch({ type: "GUARDA_BGIMAGE", photo })
        dispatch({ type: "GUARDA_ABOUT_TITLE", aboutTitle })
        dispatch({ type: "GUARDA_ABOUT_DESCRIPTION", aboutDescription })

        dispatch({ type: "GUARDA_FACEBOOK", facebook })
        dispatch({ type: "GUARDA_TWITTER", twitter })
        dispatch({ type: "GUARDA_INSTAGRAM", instagram })
        dispatch({ type: "GUARDA_YOUTUBE", youtube })

        dispatch({ type: "GUARDA_EMAIL", email })
        dispatch({ type: "GUARDA_PHONE", phone })
        dispatch({ type: "GUARDA_ADDRESS", address })

        dispatch({ type: "GUARDA_TITLE", title })
        dispatch({ type: "GUARDA_LOGO", logo })
    }
}

/**Reducer que salva os estados de content e BGimage do portal.
 * actions types: GUARDA_CONTENT, GUARDA_BGIMAGE */
export const reducerGuardaContent = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case "GUARDA_CONTENT":
            state.content = action._id
            return state
        case "GUARDA_BGIMAGE":
            state.bgHomeImage = action.photo
            return state
        case "GUARDA_ABOUT_TITLE":
            state.aboutTitle = action.aboutTitle
            return state
        case "GUARDA_ABOUT_DESCRIPTION":
            state.aboutDescription = action.aboutDescription
            return state

        case "GUARDA_FACEBOOK":
            state.objeto.facebook = action.facebook
            return state
        case "GUARDA_TWITTER":
            state.objeto.twitter = action.twitter
            return state
        case "GUARDA_INSTAGRAM":
            state.objeto.instagram = action.instagram
            return state
        case "GUARDA_YOUTUBE":
            state.objeto.youtube = action.youtube
            return state

        case "GUARDA_EMAIL":
            state.contato.email = action.email
            return state
        case "GUARDA_PHONE":
            state.contato.phone = action.phone
            return state
        case "GUARDA_ADDRESS":
            state.contato.address = action.address
            return state

        case "GUARDA_LOGO":
            state.business.logo = action.logo
            return state
        case "GUARDA_TITLE":
            state.business.title = action.title
            return state
        default:
            return state
    }
}

