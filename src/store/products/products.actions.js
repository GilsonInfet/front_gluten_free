import { message } from "../../util";
import { createProduct, deleteProduct,  getProducts } from "../../services/admin";
import history from "../../config/history";

export const PRODUCT_LIST = "PRODUCT_LIST"
export const PRODUCT_CREATE = "PRODUCT_CREATE"
export const PRODUCT_UPDATE = "PRODUCT_UPDATE"
export const PRODUCT_LOADING = "PRODUCT_LOADING"

export const productSave = (props, id = null) => {
    return async (dispatch) => {
        if (id === null) {
            const { data } = await createProduct(props)
            dispatch({ type: PRODUCT_CREATE, data });
            history.push('/admin/categorias?view=list')

        } else {
            // const { data } = await updatePRODUCT(id, props)
            // dispatch({ type: PRODUCT_UPDATE, { name: 1}, id });
        }
    };
};


/**Pede a atualização da lista de produtos em store */
export const productList = props => {
    // console.log("DISPATCHED productList")
    return async (dispatch) => {
        dispatch({ type: PRODUCT_LOADING })
        const { data } = await getProducts()
        dispatch({ type: PRODUCT_LIST, data })
        dispatch({ type: PRODUCT_LOADING })
    };
}

export const productDrop = props => {
    return async (dispatch) => {

        const { PRODUCT } = props
        dispatch({ type: PRODUCT_LOADING })

        deleteProduct(PRODUCT._id)
            .then(res => {
                if (res.status === 200) {
                    dispatch(productList())
                    dispatch({ type: PRODUCT_LOADING })
                    message('success', `Categoria excluída com sucesso.`)
                }
            }).catch(error => {
                message('error', ` ${error.response.data.error}`)
            })

    };
}
