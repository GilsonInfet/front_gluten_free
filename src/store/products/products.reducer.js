

import {
    PRODUCT_LIST,
    PRODUCT_LOADING,
    PRODUCT_CREATE
} from './products.actions'

const INITIAL_STATE = {
    products: [],
    loading: false
}

const reducer = (state = INITIAL_STATE, action) => {
    // console.log(action)
    switch (action.type) {
        case PRODUCT_LOADING:
            state.loading = !state.loading
            return state
        case PRODUCT_LIST:
            state.products = action.data
            return state
        case PRODUCT_CREATE:
            state.products.push(action.data)
            return state
        default:
            return state
    }

}

//importado por src\store\index.js
export default reducer
