//reducer e action

const INITIAL_STATE = {show :false,
    updating :false
 }



/**Dispara a alternancia de exibição do menu lateral em layout do admin */
export const toggleMenu = props =>{
    return (dispatch) =>{
        dispatch({type:"TOGGLE_MENU"})
    }
}



/**Dispara a alternancia is updating. Dispara updates gerais para atualçizar telas */
export const actionToggleUpadating = props =>{
    return (dispatch) =>{
        dispatch({type:"TOGGLE_UPDATE"})
    }
}

/**Reducer que efetiva a alternância da exibição do sidebar em admin. O Default está não exibir */
export const admMenuReducer = (state = INITIAL_STATE, action) =>{
    switch (action.type){
        case "TOGGLE_MENU":
            state.show = !state.show 
            return state
            case "TOGGLE_UPDATE":
                state.updating = !state.updating 
                return state

        default:


        return state
    }
}

