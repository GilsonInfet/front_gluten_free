import http from '../config/http'


/**
 *rota nome simples da rota
 filter exemple :  ?highlight=true
 */
const getWhatIWant = (rota, filter="" ) => {
    return !filter ?  http.get(`/${rota}`) : http.get(`/${rota}?${filter}`)
}


//CONTENT
const getContent = () => http.get('/content')

/**Patch de content */
const updateContent      = (id, data, config = {}) => http.patch(`/content/${id}`, data, config)
/**Foi definido que o patch de content deve receber um id de content para atualizar seus filhos */
const updateContentAbout = (id, data, config = {}) => http.patch(`/content/${id}`, data, config)

//USER
const createUser  = (data, config = {}) => http.post(`/user`, data, config)
const updateUser = (id, data) => http.patch(`/user/${id}`, data)
const deleteUser = (id) => http.delete(`/user/${id}`)

//CATEGORY
const createCategory = (data) => http.post(`/category`, data)
const updateCategory = (id, data) => http.patch(`/category/${id}`, data)
const deleteCategory = (id) => http.delete(`/category/${id}`)
/**Retorna a lista de categorias */
const getCategories = () => http.get('/category')


//PRODUCT
const createProduct = (data, config = {}) => http.post(`/product`, data, config)
const deleteProduct = (id) => http.delete(`/product/${id}`)
const updateProduct = (id, data) => http.patch(`/product/${id}`, data)
/**retorna a lista de produtos */
const getProducts = () => http.get('/product')
/**retorna a lista de produtos com highliught true */
const getProductsHighLighted = () => http.get('/product?highlight=true')


// SERVICE
const deleteServiceItem = (content_id, pdata) => http.delete(`/services/${content_id}`, {data : pdata})
const postServiceItem = (id, data, config = {}) => http.post(`/services/${id}`, data, config)

//INFO TODO: funções para cadastro e deletção de info
const deleteInfoItem = (content_id, pdata) => http.delete(`/infos/${content_id}`, {data : pdata})
const postInfoItem = (id, data, config = {}) => http.post(`/infos/${id}`, data, config)


//TODO: Post Banner, patch banner, deleteBanner
/** Params: nº do content e o objeto para match*/
const delete_banner = (content_id, pdata) => http.delete(`/banner/${content_id}`, {data : pdata})
const create_banner = (contentId, data, config = {}) => http.post(`/banner/${contentId}`, data, config)
/** Não foi criada uma rota getbanner. Avaliar se vou criar no back ou tratar aqui */
const get_banner = async () => {
  const valor = await http.get('/content')
  return valor.data.banner
}
/**ID do content atual, ID do banner a ser atualizado, data no body*/
const update_banner = (contentId, infoId, data, config = {}) => http.patch(`/banner/${contentId}-${infoId}`, data, config)



//Vou fazer patch??? precisa??

export {
    create_banner,delete_banner , get_banner, update_banner,
    getProducts,
    getCategories,
    deleteCategory,
    deleteProduct,
    createCategory,
    createProduct,
    updateCategory,
    updateProduct,
    getWhatIWant,
    createUser,
    deleteUser,
    updateContent,
    updateContentAbout,
    postServiceItem,
    getContent,
    deleteServiceItem,
    updateUser,
    deleteInfoItem,
    postInfoItem,
    getProductsHighLighted
}
