
//https://scalablecss.com/styled-components-global-styles/

import 'bootstrap/dist/css/bootstrap.min.css';
import { createGlobalStyle } from 'styled-components';

// PRECISA IMPORTAR

const GlobalStyle = createGlobalStyle`
 * { 
    /* // background: white;  NÃO PODE TER ESTE BACKGROUND */
    margin:0px;
    padding:0px;
    outline:0;
    -webkit-font-smoothing: antialiased;

    /* DEVE ESTAR NO BODY */
    body {
        
        background-color: #cccccc; /* Used if the image is unavailable */
        
    }

  }
`;


export default GlobalStyle