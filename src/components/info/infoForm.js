import React, { useState } from 'react'
import { Button,  Form, ProgressBar, Spinner } from 'react-bootstrap'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { getWhatIWant, postInfoItem } from '../../services/admin'
import { ContCol } from '../../util/lays'

/**Form de cadastro de novas info. São itens que aparecem em portal Home.*/
const InfoForm = (props) => {

    const [formInfo, setFormInfo] = useState({
        text: "",
        link: "",
        order: "",
        photo: ""
    })

    
    const [progress, setProgress] = useState(0)
    const [isProcessando, setisProcessando] = useState(false)

    const handleChange = (attr) => {
        const { value, name, checked } = attr.target
        const isCheck = name === 'show' 

        if (name === 'photo') {
            setFormInfo({
                ...formInfo,
                'photo': attr.target.files[0]
            })
        } else {

            setFormInfo({
                ...formInfo,
                [name]: isCheck ? checked : value
            })
        }
        return;
    }

    const submitInfo = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })
        // conversao dos dados paa formData
        let data = new FormData()

        Object.keys(formInfo)
            .forEach(key => data.append(key, formInfo[key]))

        const config = {
            onUploadProgress: function (progressEvent) {
                let successPercent = Math.round(progressEvent.loaded * 100 / progressEvent.total)
                setProgress(successPercent)
            },
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }

        const content = await  getWhatIWant('content');
        postInfoItem(content.data._id, data, config)
            .then((res) => {
                clearForm()
                message('success', `Item de Info Cadastrado com sucesso.`)
                setisProcessando(false)  
                window.location.reload()  
            })
            .catch((err) => message('error', `Erro ao cadastrar usuário.`))
            setisProcessando(false)
    }

    const clearForm = () => {
        setFormInfo({
            text: "",
            link: "",
            order: "",
            photo: ""
        })
    }

    const isNotValid = () => {
        return !formInfo.text || (!formInfo.photo) || !formInfo.photo
    }



    return (
        <>       

        <ContCol>
        <h6>Cadastro de informações. </h6>
        <br/>
            <Form.Group >
                <Form.Control autocomplete="off" type="text" onChange={handleChange} name="text" value={formInfo.text || ""} 
                placeholder="Texto da Info" />
            </Form.Group>

            <Form.Group >
                <Form.Control autocomplete="off" type="text" onChange={handleChange} name="link" value={formInfo.link || ""} 
                placeholder="Link da Info - Opcional" />
            </Form.Group>

            <Form.Group >
                <Form.Control autocomplete="off" type="number" onChange={handleChange} name="order" value={formInfo.order || ""}
                 placeholder="Ordem da Info" />
            </Form.Group>

            <hr />

            <Form.Group >              
                    <>
                        <input name="photo" type="file" onChange={handleChange} />                        
                    </>              
            </Form.Group>

      

            <Form.Group >
                <Button variant="primary" disabled={isNotValid()} onClick={submitInfo}>
                    {isProcessando ?  <Spinner animation="grow" role="status"/> : "Cadastrar"}
                </Button>
            </Form.Group>
            <br />
            {progress > 0 ? <ProgressBar striped variant="success" now={progress} /> : ""}
            <br />
            </ContCol>
        </>
    )

}

export default InfoForm

