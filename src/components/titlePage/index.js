import React from 'react'
import { Container } from 'react-bootstrap'
import styled from 'styled-components'

export default ({ title, sub }) => {
    return (
<Container>
        <ContainerTitle>
            
                <Title>  {title}   </Title>
                <Sub>    {sub}     </Sub>
           
        </ContainerTitle> 
        </Container>
    )
}


const ContainerTitle = styled.div`
background: #fac564;
padding:10px;
font-family: 'Dancing Script', cursive;
/* border-bottom: 1px solid #fac564; */
`

const Title = styled.div`
    color:black;
    font-size: 30px;
    font-weight:600;
    /* font-family: 'Dancing Script', cursive; */
`



const Sub = styled.div`
/* font-family: 'Dancing Script', cursive; */
color:black;
`