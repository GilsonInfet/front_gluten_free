import React, { useEffect } from 'react'
import { Button, Container, Spinner, Table } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { FaRegEdit, FaTrashAlt } from 'react-icons/fa'
import { bannerDrop, bannerList } from '../../store/banner/banner.actions'
import { actionGuardaContent } from '../../store/content/contentsaver'
import { Scrolable } from '../../util'
const BannerList = (props) => {

    // debugger
    //#region 
    const dispatch = useDispatch()
    const  banners_redux  = useSelector(state => state.banner.banners)  
    const isLoading = useSelector(state => state.banner.loading)  

    useEffect(() => {
       
        dispatch(bannerList())
       console.log(banners_redux)
    }, [dispatch])


    const contentAtual =  useSelector(state => state.content.content)
    useEffect(() => {
        if (contentAtual === "") {
            dispatch(actionGuardaContent())
        }
    }, [dispatch])

    
    const _deleteBanner = async (obj) => {
 
        const resultConfirmation = await Swal.fire({
            title: `Deseja excluir o banner ${obj._id} ?`,
            showCancelButton: true,
            confirmButtonText: `Sim`,
            cancelButtonText: `Não`
        })

        if (resultConfirmation.isConfirmed) {
            dispatch(bannerDrop(contentAtual, { _id: obj._id }))
        }
    }

    /**Ordena a lista por order */
    // const banners_redux_sort = banners_redux.sort(function (a, b) {
    //     if (a.order > b.order) {
    //         return 1;
    //     }
    //     if (a.order < b.order) {
    //         return -1;
    //     }
    //     return 0;
    // });

    //#endregion
    return (
        <>

            <Container>
                <hr />
                <h6>Lista dos Banners cadastrados</h6>
                <br />
                <Scrolable>
                    <Table striped hover>
                        <thead>
                            <tr>
                                <THeadItem>Banner Id</THeadItem>
                                <THeadItem>Title</THeadItem>
                                <THeadItem>Comentario </THeadItem>
                             
                                <THeadItem>Order </THeadItem>
                                <THeadItem>Foto </THeadItem>
                                <THeadItem>Ações </THeadItem>
                            </tr>
                        </thead>
                        <tbody>

                            {isLoading ? <Spinner animation="grow" role="status" /> :
                                <>
                                    {
                                        banners_redux.map((banner, i) => 
                                        {
                                            // debugger
                                          return   <tr key={i}>
                                                <TbodyItem>{banner._id}</TbodyItem>
                                                <TbodyItem>{banner.title}</TbodyItem>
                                                <TbodyItem>{banner.coment}</TbodyItem>
                                               
                                                <TbodyItem>{banner.order}</TbodyItem>
                                                <TbodyItem>
                                                    <Responsive src={banner.photo} alt="First slide" />
                                                </TbodyItem>
                                                <TbodyItem>
                                                    <div className="separe">
                                                        <ActionButton onClick={() => props.updateBanner(banner)} variant="info" size="sm"><FaRegEdit /></ActionButton>|
                                                        <ActionButton onClick={() => _deleteBanner(banner)} variant="danger" size="sm"><FaTrashAlt /></ActionButton>
                                                    </div>
                                                </TbodyItem>
                                            </tr>
                                        }
                                        
                                        )

                                    }
                                </>
                             } 

                        </tbody>
                    </Table>
                </Scrolable>
            </Container>

        </>
    )
}

export default BannerList
const TbodyItem = styled.td`
text-align:center;

    :nth-child(1){  width: 10%; }
    :nth-child(2){  width: 30%; }
    :nth-child(3){  width: 30%; }
    :nth-child(4){  width: 5%; }
    :nth-child(5){  width: 5%; }
    :nth-child(6){  width: 15%; }
    :nth-child(7){  
        width: 10%;
        .separe{
        
        display: flex;
        justify-content :space-around;
        }
    } 
`


const THeadItem = styled.th`
    background: #666;
    color:#eee;
    text-align:center;
`


const ActionButton = styled(Button)`
    padding:2px 4px;
    font-weight:500;

    :hover {
        opacity:0.1;
        transition : 2000ms;
    }
`

const Responsive = styled.img`
width: auto;
height: 60px;
margin:3px;

`


// const Scrolable = styled.div`                    
//     min-width: 100%;
//     height: auto;
//     overflow-x: auto;
//     overflow-y: scroll;    
//     padding: 20px;
// `





