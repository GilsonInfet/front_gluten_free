import React, { useState, useEffect } from 'react'
import { Button,  Form, ProgressBar, Spinner } from 'react-bootstrap'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import styled from 'styled-components'
import { ContCol } from '../../util/lays'
import {  bannerSave } from '../../store/banner/banner.actions'
import { useDispatch, useSelector } from 'react-redux'
import { getToken } from '../../config/auth'

/**No momento eu optei por não colocar um form de cadastrabanner
 * O banner hoje está exibindo uma lista de produtos highlighted
 * Da forma que o banner foi proposto em aula ele deve estar associado a um produto.
 * A lista de produtos pode ficar imensa. Entendo não ser necessário um form de banner 
 */
const BannerForm = (props) => {

    const isUpdate = Object.keys(props.update).length > 0

    const [bannerData, setbannerData] = useState({
        ...props.update,
    })
    
useEffect(() => {
    setbannerData({
        ...props.update,
    })
}, [props.update])


    const contentAtual = useSelector(state => state.content.content)   
    const [progress, setProgress] = useState(0)
    const [updatePhoto, setUpdatePhoto] = useState(false)
    const [isProcessando, setisProcessando] = useState(false)

    const dispatch = useDispatch() //possibilita pedir ações (dispatches)
    

    const handleChange = (attr) => {
        const { value, name, checked } = attr.target
        const isCheck = name === 'show' 

        if (name === 'photo') {
            setbannerData({
                ...bannerData,
                'photo': attr.target.files[0]
            })
        } else {

            setbannerData({
                ...bannerData,
                [name]: isCheck ? checked : value
            })
        }
        return;
    }

    const submitBanner = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })
 
        let data = new FormData()

        Object.keys(bannerData).forEach(key => data.append(key, bannerData[key]))
        
        if (typeof bannerData.photo === "string") {
            //se photo é != de formdata é update (patch) que não trocou a foto. Tem que ser removido do formdata           
            data.delete('photo')
        } 

        const config = {
            onUploadProgress: function (progressEvent) {
                let successPercent = Math.round(progressEvent.loaded * 100 / progressEvent.total)
                setProgress(successPercent)
            },
            headers: {
                'Content-type': 'multipart/form-data',
                'x-auth-token':getToken()
            }
        }
          dispatch(bannerSave(contentAtual, data, config, bannerData._id))
            .then((res) => {
                clearForm()
                message('success', `Banner Cadastrado com sucesso.`)
                setisProcessando(false)    
            })
            .catch((err) => message('error', `Erro ao  Banner.`))
            setisProcessando(false)
    }

    const clearForm = () => {
        setProgress(0)
        setUpdatePhoto(true)
        setbannerData({})
        props.updateBanner({})
    }

    const isNotValid = () => {
        return    !(bannerData.title  && bannerData.photo && bannerData.coment && bannerData.order ) 
    }

    const removePhoto = () => {
        setUpdatePhoto(true)
        setbannerData({
            ...bannerData,
            photo: ""
        })
    }




    return (
        <>
       

        <ContCol>
        <h6> Formulário de cadastro de Banner</h6>
            <br/>
            <Form.Group >
                <Form.Control type="text" onChange={handleChange} name="title" value={bannerData.title || ""} 
                placeholder="Título que fica logo abaixo da foto." />
            </Form.Group>

            <Form.Group >
                <Form.Control type="text" onChange={handleChange} name="coment" value={bannerData.coment || ""} 
                placeholder="Comentário abaixo do título" 
                as="textarea" rows={2}/>
            </Form.Group>


            {/* <Form.Group >
                <Form.Control as="select" custom name="direction" onChange={handleChange} value={bannerData.direction}>
                    <option value="">-----------</option>
                    <option value="LEFT">ESQUERDA</option>
                    <option value="RIGTH">DIREITA</option>
                </Form.Control>
            </Form.Group> */}

            <Form.Group >
                <Form.Control type="number" onChange={handleChange} name="order" value={bannerData.order || ""}
                 placeholder="Ordem da imagen" />
            </Form.Group>

            <hr />

            <Form.Group >
                {isUpdate && !updatePhoto ? (
                    <Pictur>
                        <img src={bannerData.photo} alt={bannerData.name} />
                        <span onClick={removePhoto}>Remover</span>
                    </Pictur>
                ) : (
                    <>
                        <input name="photo" type="file" onChange={handleChange} />                        
                    </>
                    )}
            </Form.Group>

      

            <Form.Group >
                <Button variant="primary" disabled={isNotValid()} onClick={submitBanner}>
                {isProcessando ?  <Spinner animation="grow" role="status"/> : ""}

                    {isUpdate ? `Atualizar ${props.update._id}` : "Cadastrar"}

                </Button>
            </Form.Group>


            {isUpdate ? <Button onClick={()=> props.updateBanner({})}>Cancelar Update</Button>:""}

            <br />
            {progress > 0 ? <ProgressBar striped variant="success" now={progress} /> : ""}
            <br />
            
            </ContCol>
            
        </>
    )

}

export default BannerForm



const Pictur = styled.div`

    display: flex;
    flex-direction: column;

    img{
        max-width: 200px;
        max-height: 200px;
    }

    span{
        cursor: pointer;
        color: #ccc;
        &:hover{
            color: red 
        }
    }


`
