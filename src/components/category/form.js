import React, { useState } from 'react'
import { Button, Form } from 'react-bootstrap'
import { useDispatch } from 'react-redux'

import { categorySave } from '../../store/categories/category.actions'
import { ContCol } from '../../util/lays'

const FormCategory = (props) => {
    const dispatch = useDispatch()

    const [formCategory, setFormCategory] = useState({
        ...props.update
    })
    // const typeReq = (data) => Object.keys(props.update).length > 0 ? updateCategory(props.update._id, data) : createCategory(data)

    const handleChange = (attr) => {
        const { value, name } = attr.target
        setFormCategory({
            ...formCategory,
            [name]: value
        })
        return;
    }

    const isUpdate = Object.keys(props.update).length > 0

    const submitCategory = async () => {

        try {
            await dispatch(categorySave(formCategory))

        } catch (error) {

        }
    }


    return (
        <>
            <ContCol>
                <Form.Group >
                    <Form.Control type="text" onChange={handleChange} name="name" value={formCategory.name || ""} placeholder="Nome da Categoria" />
                </Form.Group>
                <Form.Group >
                    <Form.Control type="text" onChange={handleChange} name="icon" value={formCategory.icon || ""} placeholder="Url Icone" />
                </Form.Group>
                <Button variant="primary" onClick={submitCategory}>
                    {isUpdate ? "Atualizar" : "Cadastrar"}
                </Button>
            </ContCol>
        </>
    )

}

export default FormCategory


