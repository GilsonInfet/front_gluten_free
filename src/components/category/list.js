import React, { useEffect } from 'react'
import { Button, Col, Container, Row, Spinner, Table } from 'react-bootstrap'
import styled from 'styled-components'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { FaRegEdit, FaTrashAlt } from 'react-icons/fa'

import { useDispatch, useSelector } from 'react-redux'
import { categoryDrop, categoryList } from '../../store/categories/category.actions'

//list.js
const ListCategory = (props) => {
    //#region
    const categories_redux =
        useSelector(state => state.category.categories) //seleciona e recebe os dados

    const dispatch = useDispatch() //possibilita pedir ações (dispatches)

    useEffect(() => {
 
        dispatch(categoryList())  //dispara p/ x.actions.js o pedido da lista
        
    }, [dispatch])

    const _deleteCategory = async (obj) => {

        const resultConfirmation = await Swal.fire({
            title: `Deseja excluir a categoria ${obj.name} ?`,
            showCancelButton: true,
            confirmButtonText: `Sim`,
            cancelButtonText: `Não`
        })

        if (resultConfirmation.isConfirmed) {
            dispatch(categoryDrop({ category: obj }))
        }
    }

    /**Ordena a lista de categorias */
    const ordenaCategorias = categories_redux.sort(function (a, b) {
        if (a.name > b.name) {
            return 1;
        }
        if (a.name < b.name) {
            return -1;
        }
        return 0;
    });

    //#endregion
    return (
        <Container>
<Row>
    <Col xs={12} sm={12} md={6} lg={6} xl={6} >
            <Table striped hover>
                <thead>
                    <tr>
                        <THeadItem>Nome</THeadItem>
                        <THeadItem>Ações</THeadItem>
                    </tr>
                </thead>
                <tbody>
                    {ordenaCategorias.length <= 0 ? <Spinner animation="grow" role="status" /> : ""}

                    {ordenaCategorias.map((catg, i) => (
                        <tr key={i}>
                            <TbodyItem>{catg.name}</TbodyItem>
                            
                            <TbodyItem>
                            <div className="separe">
                                <ActionButton onClick={() => props.updateCategory(catg)} variant="info" size="sm"><FaRegEdit /></ActionButton>
                                |
                                <ActionButton onClick={() => _deleteCategory(catg)} variant="danger" size="sm"><FaTrashAlt /></ActionButton>
                            </div>
                            </TbodyItem>
                            
                        </tr>
                    ))}

                </tbody>
            </Table>
            </Col>
            </Row>

        </Container>
    )
}

export default ListCategory
const TbodyItem = styled.td`
text-align:center;

    :nth-child(1){  width: 50%; }

    :nth-child(2){  
        width: 50%;
        .separe{
        
        display: flex;
        justify-content :space-around;
        }
    } 
`


const THeadItem = styled.th`
    background: #666;
    color:#eee;
    text-align:center;
    :nth-child(1){  width: 50%; }



`
const ActionButton = styled(Button)`
    padding:2px 4px;
    font-weight:500;

    :hover {
        opacity:0.4
    }
`










