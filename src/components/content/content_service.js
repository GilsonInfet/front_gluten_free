//fazendo diferente do professor, já que ele não fez este elemento e EU acho desnecessário 
//entupir de arquivos com funções pequenas

import React, { useState } from 'react'
import { Button, Container, Form, Spinner } from 'react-bootstrap'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { updateContent, getWhatIWant } from '../../services/admin'
import Service from '../../views/portal/service/index'
import FormServiceItemForm from './content_service_item'
import {ContCol} from '../../util/lays'
import ServicosHome from '../../views/portal/home/servicos'

//este arquivo terá o form de cadastro de info e a lista de info que é exibida em home portal
/**Une numa view os forms de cadastro de services, e o componente de list service */
const ContentService = (props) => {


    const [formContentService, setFormContentService] = useState({
        description: "",
        title: ""
    }, { name: 'count' })   

    const [isProcessando, setisProcessando] = useState(false)
    const [firesUpdate, setisfiresUpdate] = useState(false)

  
  
    //este handle atualiza apenas service label
    const handleChangeService = (attr) => {


        const { value, name } = attr.target
        

        setFormContentService({
            ...formContentService,
            [name]: value
        })
        return;
    }

    const submitServicesLabel = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })
        // conversao dos dados paa formData
        // let data = new FormData()

        // Object.keys(formContentService)
        //     .forEach(key => data.append(key, formContentService[key]))
        let services = {}
        let contentId = ""
        await getWhatIWant("content")
        .then((res) => {
             contentId = res.data._id
                services = {
                    'services.description': formContentService.description,
                    'services.title' : formContentService.title
                }                      
        })
        .catch((err) => console.log('Não foi possível pegar o content:', err.message))

        updateContent(contentId, services)
       .then((res) => {
                // clearForm()
                message('success', `Content Service Main Cadastrado com sucesso.`)
                setisProcessando(false)
                setisfiresUpdate(!firesUpdate)
                window.location.reload()
        })
        .catch((err) => message('error', `Erro ao cadastrar produto.`))

        setisProcessando(false)
    }

 

    const isNotValid = () => {
        return Object.keys(formContentService).some(k => typeof formContentService[k] === "string" && formContentService[k] === "")
    }


    const mainServices_label = (
        <>
           
            <h6>Cadastre a primeira e segunda linha de servicos / descrição genérica.</h6>
         
              
          
            <Form.Group >
                <Form.Control type="text" onChange={handleChangeService} name="title" value={formContentService.title || ""} placeholder="Título de Serviços" />
                <br />
                <Form.Control type="text" onChange={handleChangeService} name="description" value={formContentService.description || ""} as="textarea" rows={3} placeholder="Descrição de serviços" />
               
            </Form.Group>
 <br />
         
            <Form.Group >
                <Button variant="primary" disabled={isNotValid()} onClick={submitServicesLabel}>
                    {isProcessando ? <Spinner animation="grow" role="status" /> : "Cadastrar"}
                </Button>
            </Form.Group>


        </>
    )

    return (
        <>        
            <ContCol>
                {mainServices_label}
                <FormServiceItemForm/>
            </ContCol>

            <Container>
                <hr />
                    <h6>Atual Aparência de Services em Home Portal:</h6>
                <br />
                <ServicosHome listmode={1} refresh={firesUpdate} showDelete={true}></ServicosHome>

                {/* <Service showDelete={true} /> */}
            </Container>
        </>
    )

}

export default ContentService

