import React, { useState, useEffect } from 'react'
import { Button, Form, ProgressBar, Spinner } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { getWhatIWant, postServiceItem } from '../../services/admin'
import { actionToggleUpadating } from '../../store/adminmenu/adminmenu'


/**Formulário de cadastro de ITEM de serviço, cada um dos cards de serviços */
const FormServiceItemForm = () => {

    const [serviceItem, setServiceItem] = useState({
       })
    const dispatch = useDispatch()
       
    const [progress, setProgress] = useState(0)
    const [isProcessando, setisProcessando] = useState(false)
    const [atualize, setatualize] = useState(0)

    useEffect(() => {
        console.log("progress", progress)
    }, [progress])


    const handleChange = (attr) => {
        const { value, name } = attr.target

        if (name === 'photo') {
            setServiceItem({
                ...serviceItem,
                'photo': attr.target.files[0]
            })
        } else {

            setServiceItem({
                ...serviceItem,
                [name]: value
            })
        }
        return;
    }

    const submitServiceItem = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })
        // conversao dos dados paa formData
        let data = new FormData()

        Object.keys(serviceItem)
            .forEach(key => data.append(key, serviceItem[key]))

        const config = {
            onUploadProgress: function (progressEvent) {
                let successPercent = Math.round(progressEvent.loaded * 100 / progressEvent.total)
                setProgress(successPercent)
            },
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }
        const contentId = await getWhatIWant("content")

        postServiceItem(contentId.data._id, data, config)
            .then((res) => {
                clearForm()
                message('success', `Item de serviço Cadastrado com sucesso.`)
                setisProcessando(false)
                setatualize(atualize + 1)
                dispatch(actionToggleUpadating())
                // window.location.reload()
           })
            .catch((err) => message('error', `Erro ao cadastrar item de serviço.`))
            setisProcessando(false)
    }

    const clearForm = () => {
        setProgress(0)
        setServiceItem({
            order: "",
            description: "",
            photo: ""
        })
    }

    //Testa se alguma chave do objeto é uma string vazia e se sim não é validado
    const isNotValid = () => {
        return Object.keys(serviceItem).some(k => typeof serviceItem[k] === "string" && serviceItem[k] === "") || (!serviceItem.photo)
    }



    return (
        <>
            <hr />  
            <h6>Cadastre um item de serviço:</h6>     

            <Form.Group >
                <Form.Control type="text" onChange={handleChange} name="description" value={serviceItem.description || ""}  placeholder="Descrição do item de serviço" />
            </Form.Group>
            
            <Form.Group >
                <Form.Control type="text" onChange={handleChange} name="detail" value={serviceItem.detail || ""} as="textarea" rows={3} placeholder="Descrição detalhada." />
            </Form.Group>

            <Form.Group >
                <Form.Control type="number" onChange={handleChange} name="order" value={serviceItem.order || ""} placeholder="ordem" />
            </Form.Group>

            <Form.Group >
                    <>
                        <input name="photo" type="file" onChange={handleChange} />                        
                    </>
         
            </Form.Group>

            <Form.Group >
                <Button variant="primary" disabled={isNotValid()} onClick={submitServiceItem}>
                {isProcessando ?  <Spinner animation="grow" role="status"/> : "Cadastrar"}
                </Button>
            </Form.Group>
            <br />
            {progress > 0 ? <ProgressBar striped variant="success" now={progress} /> : ""}
            <br />

        </>
    )

}

export default FormServiceItemForm


