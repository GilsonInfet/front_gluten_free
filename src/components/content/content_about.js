//fazendo diferente do professor, já que ele não fez este elemento e EU acho desnecessário 
//entupir de arquivos com funções pequenas

import React, { useState } from 'react'
import { Button, Container, Form, ProgressBar,  Col, Spinner } from 'react-bootstrap'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { updateContentAbout, getWhatIWant } from '../../services/admin'
import AboutHome from '../../views/portal/home/about'



//este arquivo terá o form de cadastro de info e a lista de info que é exibida em home portal

const FormContentAbout = () => {



    const [formContentAbout, setFormContentAbout] = useState({
        photo: "",
        title: "",
        description: "",
        direction: "RIGHT"
    })


    const [isProcessando, setisProcessando] = useState(false)
    const [progress, setProgress] = useState(0)
 
    const handleChangeAbout = (attr) => {
        const { value, name, checked } = attr.target
        const isCheck = name === 'status' || name === 'highlight'

        if (name === 'photo') {
            setFormContentAbout({
                ...formContentAbout,
                'photo': attr.target.files[0]
            })
        } else {

            setFormContentAbout({
                ...formContentAbout,
                [name]: isCheck ? checked : value
            })
        }
        return;
    }


    //este handle atualiza apenas service label
    const handleChangeService = (attr) => {


        const { value, name } = attr.target
        console.log(name, value)

        setFormContentAbout({
            ...formContentAbout,
            [name]: value
        })
        return;
    }

    const submitAbout = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })

        // conversao dos dados paa formData
        let data = new FormData()

        Object.keys(formContentAbout)
            .forEach(key => data.append(['about.' + key], formContentAbout[key]))

        const config = {
            onUploadProgress: function (progressEvent) {
                let successPercent = Math.round(progressEvent.loaded * 100 / progressEvent.total)
                setProgress(successPercent)
            },
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }

        const contentId = await getWhatIWant("content")

            updateContentAbout(contentId.data._id, data, config)        
            .then((res) => {
                clearForm()
                message('success', `Produto Cadastrado com sucesso.`)
                setisProcessando(false)
                window.location.reload()
            })
            .catch((err) => message('error', `Erro ao cadastrar produto.`))
        setisProcessando(false)
    }

    const clearForm = () => {
      
        setFormContentAbout({
            photo: "",
            title: "",
            description: "",
            direction: "RIGHT"
        })
    }

    const isNotValid = () => {
        return Object.keys(formContentAbout).some(k => typeof formContentAbout[k] === "string" && formContentAbout[k] === "")
    }


    /** 'about.title': 'about.title',
      'about.description': 'about.description',
      'about.direction': 'LEFT',
      'about.photo': 'about/bigbang.jpg'  
       */



    return (
        <>

        <Container>
            <Col xs={12} sm={12} md={12} lg={8} xl={6} >
            <h6>Cadastro da área "sobre" exibido em Home do Portal.</h6>
            <hr />

            <Form.Group  >
                <Form.Control type="text" onChange={handleChangeService} name="title" 
                value={formContentAbout.title || ""} placeholder="Título Sobre" />
                <br />
                <Form.Control type="text" onChange={handleChangeService} name="description" 
                value={formContentAbout.description || ""} as="textarea" rows={3} placeholder="Sobre descrição" />
                <br />
                
                <Form.Control as="select" custom name="direction" onChange={handleChangeService} value={formContentAbout.direction}>
                    <option value="RIGHT">IMAGEM DE ABOUT À DIREITA</option>                    
                    <option value="LEFT">IMAGEM DE ABOUT À ESQUERDA</option>
                </Form.Control>

                <br />

            </Form.Group>

            <Form.Group >
                <input name="photo" type="file" onChange={handleChangeAbout} />
            </Form.Group>



            <Form.Group >
                <Button variant="primary" disabled={isNotValid()} onClick={submitAbout}>
                    {isProcessando ? <Spinner animation="grow" role="status" /> : "Cadastrar"}
                </Button>
            </Form.Group>

            <br />
            {progress > 0 ? <ProgressBar striped variant="success" now={progress} /> : ""}
      
            <hr />
            <h6>Aparência atual do componente  "Sobre" no Portal </h6>
            <br />
            </Col>
            <AboutHome />
            </Container>

            
        </>
    )

}

export default FormContentAbout



