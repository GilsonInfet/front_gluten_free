import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import ItemMenu from './itemMenu'
import { IconContext } from 'react-icons';
import * as FaIcons from 'react-icons/fa';
import './navbar.css';
import { useDispatch, useSelector } from 'react-redux';
import { toggleMenu } from '../../../store/adminmenu/adminmenu';
import styled from 'styled-components';
import { TitleComponent } from '../../../util/TitleComponent';

const Sidebar = () => {
    
    const businessTitle = useSelector(state => state.content.business.title)
    const sidebar = useSelector(state=> state.admmenu.show)
    const dispatch = useDispatch()
    const showSidebar =()=> {dispatch(toggleMenu()) }

    //links de sidebar são montados aqui. Em outra formação ele tinha deixado os itens formados dinamicamente
    return (
<div>

        <IconContext.Provider value={{ color: '#fff' }}>     

            <ul onClick={showSidebar}
                className={sidebar ? "nav-menu active navbar-nav bg-gradient-primary sidebar sidebar-dark accordion": 'nav-menu navbar-nav bg-gradient-primary sidebar sidebar-dark accordion'} id="accordionSidebar" >

                <Link to={'/'} className="sidebar-brand d-flex align-items-center justify-content-center" href="index.html" >
                    <div className="sidebar-brand-icon rotate-n-15">
                        <i className="fas fa-laugh-wink" />
                    </div>
    <div className=" mx-3">{businessTitle}</div>
                </Link >
               
                < hr className="sidebar-divider " />
               
                <ItemMenu className="leftspan"  link="/admin" name="Home Admin" />
                <ItemMenu  link="/admin/categorias" name="Categorias" />
                <ItemMenu  link="/admin/produtos" name="Produtos" />

                < hr  className="sidebar-divider " />

                <ItemMenu  link="/admin/banner" name="Banner" />
                <ItemMenu  link="/admin/content-about" name="Content About" />
                <ItemMenu  link="/admin/content-service" name="Content Service" />
                <ItemMenu  link="/admin/equipe" name="Equipe" />
                <ItemMenu  link="/admin/info" name="Informações" />
                <ItemMenu  link="/admin/homeconfig" name="Config Gerais" />
                
            </ul >
        </IconContext.Provider>
        </div>
    )
}

//importado por src\components\Layout\admin\index.js
export default Sidebar

