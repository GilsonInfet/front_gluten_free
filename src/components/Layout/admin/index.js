import React from 'react'
import '../../../assets/css/sb-admin-2.min.css'
import SideBar from './sidebar'
import AdminHeader from './header'
import AdminFooter from './footer' // O professor não quis colocar footer na view admin eu deixei
import styled from 'styled-components'
import { TitleComponent } from '../../../util/TitleComponent'

const LayoutAdmin = ({ children, Menu }) => {
    return (

            <div id="wrapper">
                <TitleComponent title="Admin" />
                <SideBar Menu={Menu}></SideBar>
                <div id="content-wrapper" className="d-flex flex-column">
                    <div id="content">
        <Minheigth>
                        <AdminHeader />
                        <div className="container-fluid">
                            {children}                            
                        </div>
            </Minheigth>                    
                    </div>   

                    <AdminFooter />
                </div>               
            </div>


    )
}

//importado por \src\views\admin\index.js.
//uma vez que este arquivo se chama index, será o primeiro lido nesta pasta. Quem quiser importar o componente deste arquivo não
//não precisa respeitar o nome de componente declarado aqui.
export default LayoutAdmin

// recebe como children o componente do dashboard:  src\views\admin\dash.js

const Minheigth = styled.div`
min-height : 190vh;
`