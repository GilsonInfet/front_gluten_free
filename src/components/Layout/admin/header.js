import React from 'react'
import { Button, Nav, NavDropdown } from 'react-bootstrap'
import * as FaIcons from 'react-icons/fa';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { getUser, removeToken } from '../../../config/auth.js'
import history from '../../../config/history.js'
import { toggleMenu } from '../../../store/adminmenu/adminmenu.js';


const Header = () => {


const sair = () =>{
    console.log("sair")
    removeToken()
    history.push("/")
}

const dispatch = useDispatch()

const showSidebar =()=> {dispatch(toggleMenu()) }

    return (
    
        <Nav className="mr-auto navbar navbar-expand bg-white mb-4">
     
              <div className='navbarx'>
          <Link to='#' className='menu-bars'>
            <FaIcons.FaBars onClick={showSidebar} />
          </Link>
        </div>
 
            <NavDropdown title={getUser().name} id="dropdown-basic" className="ml-auto">
                <NavDropdown.Item onClick={()=>sair()} href="/admin">Sair</NavDropdown.Item>
            </NavDropdown>
        </Nav>
    )
}
export default Header
