import React from 'react'


//o professor não colocou o footer no projeto dele
const AdminFooter = () => {
    return (
        <footer className="sticky-footer bg-white">
            <div className="container my-auto">
                <div className="copyright text-center my-auto">
                    <span>Site desenvolvido por <a href="https://gilsonpaulo.com.br/">GPWEB</a></span>
                </div>
            </div>
        </footer>

    )
}

export default AdminFooter
