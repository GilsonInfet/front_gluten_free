import React, { useEffect } from 'react'
import styled, { createGlobalStyle } from 'styled-components';
import Header from './header'
import Footer from './footer'
import { Sharegroup } from '../../../util/shares';
import { useDispatch, useSelector } from 'react-redux';
import { actionGuardaContent } from '../../../store/content/contentsaver';
import { TitleComponent } from '../../../util/TitleComponent';

const Layout = ({ children }) => {
    const dispatch = useDispatch()
    
    const bgimage = useSelector(state => state.content.bgHomeImage)
    useEffect(() => {      
            dispatch(actionGuardaContent())
    }, [dispatch])    

    return (
        <>
         <TitleComponent title="Portal" />
        <GlobalStyle bgi={bgimage}/>
            <HeaderContainer>
                <Header />
            </HeaderContainer>

            <Content>
            <Sharegroup/>

            {children}

            </Content>

            <FooterContainer>
                <Footer />
            </FooterContainer>
        </>
    )
}

const Content = styled.div`
min-height:300px;
`

const HeaderContainer = styled.div`
background: blue;
width:100%;
`


const FooterContainer = styled.div`
background:brown;
height:200px;
width:100%;
`

const GlobalStyle = createGlobalStyle`
 * { 
    margin:0px;
    padding:0px;
    outline:0;
    -webkit-font-smoothing: antialiased;

    /* DEVE ESTAR NO BODY */
    body {
       background-image: url(${props => props.bgi }) ; 
        background-color: #cccccc; /* Used if the image is unavailable */
        height: 500px; /* You must set a specified height */
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
}
  }
`;

export default Layout
