import React, { useState } from 'react'
import { Col, Container, Nav, Navbar, Row } from 'react-bootstrap';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';


export default () => {
    const logo = useSelector(state => state.content.business.logo)

    const businessTitle = useSelector(state => state.content.business.title)

    const linksNavegacao = [
        {
            title: "Home",
            link: "/",
            icon: ""
        },
        {
            title: "Sobre",
            link: "sobre",
            icon: ""
        },
        {
            title: "Produto",
            link: "produtos",
            icon: ""
        },
        {
            title: "Serviços",
            link: "servicos",
            icon: "g"
        },
        {
            title: "Contato",
            link: "contato",
            icon: ""
        },

    ]

    const [expanded, setExpanded] = useState(false);

    /**
     * Solução do colapse after click em celular por https://stackoverflow.com/a/58530447/13702866
     */
    return (
        <Header>
            <Container>
                <Navbar expanded={expanded} variant="dark" expand="lg" >

                    <Navbar.Brand className="logo" >

                        <NavLink exact={true} to="/admin" >
                            <Nav.Link as="div" >
                                <Row> 
                                    <div className="doflex">
                                <Responsive src={logo} alt="First slide" />
                                <div className="titleLogo">{businessTitle}</div>
                                </div>
                                </Row>
                            </Nav.Link>
                        </NavLink>

                    </Navbar.Brand>
                    <div className="maxw" ></div>
                    <Navbar.Toggle onClick={() => setExpanded(expanded ? false : true)} aria-controls="justify-content-end basic-navbar-nav" />

                    <Navbar.Collapse className="justify-content-end" >
                        <Nav defaultActiveKey={false} className="mr-auto">

                            {linksNavegacao.map((item, i) => (
                                <NavLink exact={true} to={item.link} key={i}>
                                    <Nav.Link onClick={() => setExpanded(false)} as="div" > {item.title}</Nav.Link>
                                </NavLink>
                            ))}
                        </Nav>

                    </Navbar.Collapse>

                </Navbar>

            </Container>
        </Header>
    )
}

const Header = styled.div`
background-color:#111;
.doflex{
    display: flex;
}
.titleLogo{
display: flex;
align-items: flex-end;

}
.maxw{
width:60%;
/* border: solid 1px red;   */
}

@media screen and (max-width: 991px) {
        .maxw { display: none;  } 
}

.logo{
    font-family: 'Dancing Script', cursive;
    font-size:25px;
    font-weight:100;
}

.nav-link:hover{
    color:#fac564!important;
    font-weight:500;
    /* font-family: 'Dancing Script', cursive; */
}

.nav-link{
    font-size:20px;
    font-family: 'Dancing Script', cursive;
}

svg{
    margin: 1px;
    font-size:25px;
    color:#fac564;   
}
`
const Responsive = styled.img`
width: 40px;
height: 40px;
margin:3px;

`