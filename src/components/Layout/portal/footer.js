import React, { useEffect } from 'react'

import styled from 'styled-components';
import { TiSocialFacebook, TiSocialTwitter, TiSocialYoutube } from 'react-icons/ti'

import { AiFillInstagram } from 'react-icons/ai'
import { Col, Container, Row } from 'react-bootstrap'
import ToTopButton from '../topButton';
import { useDispatch, useSelector } from 'react-redux';

const Footer = () => {


    const dispatch = useDispatch()
    const sobrNosTitle = useSelector(state => state.content.aboutTitle)
    const sobrNosDescription = useSelector(state => state.content.aboutDescription)

    const facebook = useSelector(state => state.content.objeto.facebook)
    const twitter = useSelector(state => state.content.objeto.twitter)
    const instagram = useSelector(state => state.content.objeto.instagram)
    const youtube = useSelector(state => state.content.objeto.youtube)

    const email = useSelector(state => state.content.contato.email)
    const phone = useSelector(state => state.content.contato.phone)
    const address = useSelector(state => state.content.contato.address)

    useEffect(() => {
        // dispatch(actionGuardaContent())
    }, [dispatch])

    return (
        <Footerx>
            <br />
            <FooterInfo>
                <Container >
                    <Row>
                        <Col className="bg" xs={12} md={6}>
                            <div className='titulo'>{sobrNosTitle}</div>
                            <p className='aboutUs'>
                                {sobrNosDescription}
                            </p>

                        </Col>


                        <Col className="bg" xs={12} md={6}>
                            <div className='titulo'>Fale Conosco:</div>
                            <ul >
                                <li>
                                    <a href={phone}> {`Tel.: ${phone}`} </a>
                                </li>


                                <li><a href={`mailto: ${email}`}>{email}</a></li>
                                <li>{address}</li>
                            </ul>
                        </Col>
                    </Row>
                </Container>
            </FooterInfo>

            <FooterSocial>
                <Container >
                    {facebook ? <a href={facebook} target="_blank" rel="noopener noreferrer"><TiSocialFacebook /> </a> : ""}
                    {instagram ? <a href={instagram} target="_blank" rel="noopener noreferrer"><AiFillInstagram /> </a> : ""}
                    {twitter ? <a href={twitter} target="_blank" rel="noopener noreferrer"><TiSocialTwitter /> </a> : ""}
                    {youtube ? <a href={youtube} target="_blank" rel="noopener noreferrer" ><TiSocialYoutube /> </a> : ""}
                </Container>
            </FooterSocial>

            <FooterCopy>
                <Container >
                    Todos os direitos reservados.
                </Container>
                <hr />
                <Container >
                    Site desenvolvido por <a href="https://gilsonpaulo.com.br/">GPWEB</a>
                </Container>
                <br />
            </FooterCopy>
            <ToTopButton />
        </Footerx>
    )
}

export default Footer

const Footerx = styled.div`
background:black;
padding: 150px auto;
margin-bottom:120px;
border-top: solid 2px #fac564;
font-size: 20px;
font-family: 'Dancing Script', cursive;
`

const FooterInfo = styled.div`

.bg{
background:black;

color:#808080;
/* height: 150px; */
/* color:white; */
}

.titulo{
    padding: 5px 0;
    font-size:20px;
    font-weight:100;
    color:rgb(250, 197, 100);
    border-bottom: thin solid #ddd;
    margin-bottom:10px;
}


`

const FooterSocial = styled.div`
width:100%;
height:40px;
background:black;
border-top: thin solid grey;
display: flex;
align-items: flex-top;
color:white;
padding: 10px 0;
cursor:pointer;
svg{
    margin: 5px;
    font-size:25px;
    :hover{
        color:yellow;
    }
}


a svg{
    color:rgb(250, 176, 45);

}
`

const FooterCopy = styled.div`
width:100%;

background:black;
color:white;
padding: 10px ;

text-align: center;
a{
    color:rgb(250, 176, 45);
}
`
const Responsive = styled.img`
width: auto;
height: 60px;
margin:3px;

`