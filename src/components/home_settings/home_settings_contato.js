import React, { useEffect, useState } from 'react'
import { Button, Form, Spinner } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { getToken } from '../../config/auth'

import { actionAtualizaContent, actionGuardaContent } from '../../store/content/contentsaver'
import { ContCol } from '../../util/lays'

const HomeSettingsContato = (props) => {

    const contentAtual = useSelector(state => state.content.content)
    const [isProcessando, setisProcessando] = useState(false)
    const dispatch = useDispatch() //possibilita pedir ações (dispatches)

    useEffect(() => {
        if (contentAtual === ""){
            dispatch(actionGuardaContent())
        }
    }, [dispatch])

    const [content, setContents] = useState({ })

    const clearForm = () => {
        setContents({})
    }

    const isNotValid = () => {
        return ! (content.address && content.email && content.phone)
    }

    const submitContato = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })
        // conversao dos dados paa formData
        let data = new FormData()

        Object.keys(content).forEach(key => data.append(['contato.' + key], content[key]))

    
        dispatch(actionAtualizaContent(contentAtual, data ))
            .then((res) => {
                clearForm()
                message('success', `Dados de contato cadastrados com sucesso.`)
                setisProcessando(false)
            })
            .catch((err) => message('error', `Erro no cadastro de dados de contato: ${err.message}`))
        setisProcessando(false)
    }

    const handleChange = (attr) => {
        const { value, name, checked } = attr.target
        const isCheck = name === 'show'

        if (name === 'photo') {
            setContents({
                ...content,
                'photo': attr.target.files[0]
            })
        } else {

            setContents({
                ...content,
                [name]: isCheck ? checked : value
            })
        }
        return;
    }

    return (
        <>            
            <ContCol>

                <h6>Dados de Contato.</h6>
                <Form.Group >
                    <Form.Control type="text" onChange={handleChange} name="address" value={content.address || ""}
                        placeholder="Endereço do negócio." />
                </Form.Group>

                <Form.Group >
                    <Form.Control type="text" onChange={handleChange} name="email" value={content.email || ""}
                        placeholder="Email do estabelecimento." />
                </Form.Group>

                <Form.Group >
                    <Form.Control type="number" onChange={handleChange} name="phone" value={content.phone || ""}
                        placeholder="Telefone do estabelecimento. (Apenas números)." />
                </Form.Group>
                
                <Form.Group >
                    <Button variant="primary" disabled={isNotValid()} onClick={submitContato}>
                        {isProcessando ? <Spinner animation="grow" role="status" /> : "Cadastrar"}
                    </Button>
                </Form.Group>
                               
       
                <br />

            </ContCol>

        </>
    )
}

export default HomeSettingsContato
