import React, { useEffect, useState } from 'react'
import { Button, Form, ProgressBar, Spinner } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { getToken } from '../../config/auth'

import { actionAtualizaContent, actionGuardaContent } from '../../store/content/contentsaver'
import { ContCol } from '../../util/lays'

const HomeSettingsLogo = () => {

    const contentAtual = useSelector(state => state.content.content)
    const [progress, setProgress] = useState(0)
    const [isProcessando, setisProcessando] = useState(false)
    const dispatch = useDispatch() //possibilita pedir ações (dispatches)

 
    useEffect(() => {
        if (contentAtual === ""){
            dispatch(actionGuardaContent())
        }
    }, [dispatch])

    const [content, setContents] = useState({  })

    const clearForm = () => {
        setContents({})
    }


    const isNotValid = () => {
        return ! (content.title  && content.photo)
    }


    const submitBanner = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })
        // conversao dos dados paa formData
        let data = new FormData()

        Object.keys(content).forEach(key => data.append([`business.` + key], content[key]))

        
        const config = {
            onUploadProgress: function (progressEvent) {
                let successPercent = Math.round(progressEvent.loaded * 100 / progressEvent.total)
                setProgress(successPercent)
            },
            headers: {
                'Content-type': 'multipart/form-data',
                'x-auth-token': getToken()
            }
        }
        dispatch(actionAtualizaContent(contentAtual, data, config))
            .then((res) => {
                clearForm()
                message('success', `Cadastrado com sucesso.`)
                setisProcessando(false)
                setProgress(0)
            })
            .catch((err) => message('error', `Erro ao  Banner.`))
        setisProcessando(false)
    }

    const handleChange = (attr) => {
        const { value, name, checked } = attr.target
        const isCheck = name === 'show'

        if (name === 'photo') {
            setContents({
                ...content,
                'photo': attr.target.files[0]
            })
        } else {

            setContents({
                ...content,
                [name]: isCheck ? checked : value
            })
        }
        return;
    }

    return (
        <>            
            <ContCol>

              
                <h6>Nome e Logo do negócio.</h6>
                <Form.Group>
                    <Form.Control type="text" onChange={handleChange} name="title" value={content.title || ""}
                        placeholder="Nome do Negócio." />
                </Form.Group>

                <Form.Group >
                    <input name="photo" type="file" onChange={handleChange} />
                </Form.Group>

                <Form.Group >
                    <Button variant="primary" disabled={isNotValid()} onClick={submitBanner}>
                        {isProcessando ? <Spinner animation="grow" role="status" /> : "Cadastrar"}
                    </Button>
                </Form.Group>
                {progress > 0 ? <ProgressBar striped variant="success" now={progress} /> : ""}
            </ContCol>
            <br />
        </>
    )
}

export default HomeSettingsLogo


