import React, { useEffect, useState } from 'react'
import { Button,  Form, ProgressBar,  Spinner } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { getToken } from '../../config/auth'
import { actionAtualizaContent, actionGuardaContent } from '../../store/content/contentsaver'
import { ContCol } from '../../util/lays'

const HomeSettingsBgImage = () => {

    const contentAtual = useSelector(state => state.content.content)
    const [progress, setProgress] = useState(0)
    const [isProcessando, setisProcessando] = useState(false)
    const dispatch = useDispatch() 

    const [content, setContents] = useState({ })

    useEffect(() => {
        if (contentAtual === ""){
            dispatch(actionGuardaContent())
        }
    }, [dispatch])


    const clearForm = () => {
        setContents({})
    }

    const isNotValid = () => {
        return !(content.photo)
    }


    const submitBgImage = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })
        // conversao dos dados paa formData
        let data = new FormData()

        Object.keys(content).forEach(key => data.append(key, content[key]))

 
        const config = {
            onUploadProgress: function (progressEvent) {
                let successPercent = Math.round(progressEvent.loaded * 100 / progressEvent.total)
                setProgress(successPercent)
            },
            headers: {
                'Content-type': 'multipart/form-data',
                'x-auth-token': getToken()
            }
        }

        console.log(contentAtual, data, config)

        dispatch(actionAtualizaContent(contentAtual, data, config))
            .then((res) => {
                clearForm()
                message('success', `Imagem de fundo cadastrada com sucesso.`)
                setisProcessando(false)
                setProgress(0)
            })
            .catch((err) => message('error', `Erro no cadastro da imagem de fundo.`))
        setisProcessando(false)
    }

    const handleChange = (attr) => {

            setContents({
                ...content,
                'photo': attr.target.files[0]
            })

        return;
    }

    return (
        <>            
            <ContCol>
                <h6>Imagem do fundo.</h6>
                <br/>
                <Form.Group >
                    <input name="photo" type="file" onChange={handleChange} />
                </Form.Group>
                <Form.Group >
                    <Button variant="primary" disabled={isNotValid()} onClick={submitBgImage}>
                        {isProcessando ? <Spinner animation="grow" role="status" /> : "Cadastrar"}
                    </Button>
                </Form.Group>
                {progress > 0 ? <ProgressBar striped variant="success" now={progress} /> : ""}
                <br />
            </ContCol>

        </>
    )
}

export default HomeSettingsBgImage


