import React, { useEffect, useState } from 'react'
import { Button,  Form, Spinner } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { getToken } from '../../config/auth'
import { actionAtualizaContent, actionGuardaContent } from '../../store/content/contentsaver'
import { ContCol } from '../../util/lays'


const HomeSettingsSocial = (props) => {

    const contentAtual = useSelector(state => state.content.content)
    const [isProcessando, setisProcessando] = useState(false)
    const dispatch = useDispatch() //possibilita pedir ações (dispatches)

    useEffect(() => {
        if (contentAtual === ""){
            dispatch(actionGuardaContent())
        }
    }, [dispatch])

    const [content, setContents] = useState({
        facebook : "",
        instagram : "",
        youtube : "",
        twitter : ""
    })

    const clearForm = () => {
        setContents({})
    }

    const isNotValid = () => {
        return !content.facebook && !content.instagram && !content.youtube && !content.twitter
    }

 
    const submitSocial = async (zerar=false) => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })
        // conversao dos dados paa formData
        let data = new FormData()

        

        Object.keys(content).forEach(key => data.append([`social.`+key], zerar ? "": content[key]))


        const config = {
            headers: {
                'Content-type': zerar ? 'application/json' : 'multipart/form-data',
                'x-auth-token': getToken()
            }
        }


      
        
        if(zerar ){
           data = { social: { facebook: '', instagram: '', twitter: '', youtube: '' } }
          
        }

        dispatch(actionAtualizaContent(contentAtual, data, config))
            .then((res) => {
                clearForm()
                message('success', `Link Cadastrado com sucesso.`)
                setisProcessando(false)
            })
            .catch((err) => message('error', `Erro no cadastro do Link.`))
        setisProcessando(false)
    }

    const handleChange = (attr) => {
        const { value, name, checked } = attr.target
        const isCheck = name === 'show'

        if (name === 'photo') {
            setContents({
                ...content,
                'photo': attr.target.files[0]
            })
        } else {

            setContents({
                ...content,
                [name]: isCheck ? checked : value
            })
        }
        return;
    }

    const style = {
        margem : {
            margin : "10px"
            }
    }


const resetLinks = async ()=>{



}

    return (
        <>            
            <ContCol>
                <h6>Redes Sociais.</h6>
                <Form.Group >                   
                    <Form.Control type="text" onChange={handleChange} name="facebook" value={content.facebook || ""}
                                placeholder="Link do Facebook" />                     
                </Form.Group>

                <Form.Group >
                    <Form.Control type="text" onChange={handleChange} name="instagram" value={content.instagram || ""}
                        placeholder="Link do Instagram" />
                </Form.Group>

                <Form.Group >
                    <Form.Control type="text" onChange={handleChange} name="youtube" value={content.youtube || ""}
                        placeholder="Link do Youtube" />
                </Form.Group>

                <Form.Group >
                    <Form.Control type="text" onChange={handleChange} name="twitter" value={content.twitter || ""}
                        placeholder="Link do Twitter" />
                </Form.Group>

                <Form.Group >
                    <Button variant="primary" disabled={isNotValid()} onClick={()=> submitSocial(false)}>
                        {isProcessando ? <Spinner animation="grow" role="status" /> : "Cadastrar"}
                    </Button>
                {/* </Form.Group>  

                <Form.Group > */}
                    <Button style={style.margem} variant="primary" onClick={()=> submitSocial(true)}>
                        {isProcessando ? <Spinner animation="grow" role="status" /> : "Reiniciar Links"}
                    </Button>
                </Form.Group> 

            </ContCol>
            <br />
        </>
    )
}

export default HomeSettingsSocial


const Pictur = styled.div`

    display: flex;
    flex-direction: column;

    img{
        max-width: 200px;
        max-height: 200px;
    }

    span{
        cursor: pointer;
        color: #ccc;
        &:hover{
            color: red 
        }
    }


`
