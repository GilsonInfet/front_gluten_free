import React from 'react'
import { Container } from 'react-bootstrap'
import styled from 'styled-components'
import HomeSettingsBgImage from './home_settings_bgimage'
import HomeSettingsContato from './home_settings_contato'
import HomeSettingsLogo from './home_settings_logo'
import HomeSettingsSocial from './home_settings_social'

const HomeSettings = (props) => {


    return (
        <Container>
            <HomeSettingsBgImage />
            <hr/>
            <HomeSettingsContato />
            <hr/>
            <HomeSettingsLogo />
            <hr/>
            <HomeSettingsSocial />
        </Container>
    )
}

export default HomeSettings


const Pictur = styled.div`

    display: flex;
    flex-direction: column;

    img{
        max-width: 200px;
        max-height: 200px;
    }

    span{
        cursor: pointer;
        color: #ccc;
        &:hover{
            color: red 
        }
    }


`
