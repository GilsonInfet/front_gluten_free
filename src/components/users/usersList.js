import React, { useEffect } from 'react'
import { Button, Spinner, Table } from 'react-bootstrap'
import { FaRegEdit, FaTrashAlt } from 'react-icons/fa'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import { userDrop, userListAll } from '../../store/users/users.actions'
import { swalConfirmation } from '../../util'


/**Userlist do painel do admin */
const UsersList = (props) => {  

        const users_redux = useSelector(state => state.user.usersAll)
        const dispatch = useDispatch()
        useEffect(() => {
            dispatch(userListAll())
        }, [dispatch, users_redux])
    
    const apagarUsuario = (user) =>{
                swalConfirmation(`Excluir o usuário ${user.name}?`,
                 ()=>{dispatch(userDrop(user._id))
                
                } )
    }
    
    
      return (
        <>
            <h6>Lista de todos os usuários</h6>
            <Scrolable>
            <Table striped hover>
                    <thead>
                        <tr>
                            <THeadItem>Nome</THeadItem>
                            <THeadItem>Função</THeadItem>
                            <THeadItem>Email</THeadItem>
                            <THeadItem>Id</THeadItem>
                            <THeadItem>Exibição</THeadItem>
                            <THeadItem>Briefing</THeadItem>
                            <THeadItem>Ações</THeadItem>
                            
                        </tr>
                    </thead>
                    <tbody>
    
                        {users_redux.length <= 0 ? <Spinner animation="grow" role="status" /> : ""}
    
                        {users_redux.map((catg, i) => (
                            <tr key={i}>
                                <td>{catg.name}</td>
                                <td>{catg.funcao}</td>
                                <td>{catg.email}</td>
                                <td>{catg._id}</td>
                                <td>{catg.show ? "SIM" : "--"}</td>
                                <td>{catg.briefing}</td>
                                <TDItem>
    
                                    <ActionButton onClick={() => props.updateUser(catg)} variant="info" size="sm"><FaRegEdit /></ActionButton>
                                    |

                                    <ActionButton onClick={() => apagarUsuario(catg)} variant="danger" size="sm"><FaTrashAlt /></ActionButton>
                                </TDItem>
                            </tr>
                        ))}
    
                    </tbody>
                </Table>
                </Scrolable>
        </>
      )
    }
    


export default UsersList

const Scrolable = styled.div`                    
    min-width: 100%;
    height: auto;
    overflow-x: auto;
    overflow-y: auto;    
    padding: 20px;
`
const THeadItem = styled.th`
    background: #666;
    color:#eee;

    :nth-child(1){  width: 10%; }
    :nth-child(2){  width: 10%; }
    :nth-child(3){  width: 5%; }
    :nth-child(6){  width: 50%; }
    :nth-child(7){  width: 20%; }
`

const TDItem = styled.td`
    display: flex;
    justify-content :center;
`

const ActionButton = styled(Button)`
    padding:2px 4px;
    font-weight:500;

    :hover {
        opacity:0.4
    }
`