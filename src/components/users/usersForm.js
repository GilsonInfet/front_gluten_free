import React, { useState, useEffect } from 'react'
import { Button, ButtonGroup, Form, ProgressBar, Spinner, ToggleButton } from 'react-bootstrap'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { createUser, updateUser } from '../../services/admin'
import { FaCheck, FaTimes } from 'react-icons/fa'
import styled from 'styled-components'
import { ContCol } from '../../util/lays'


const UsersForm = (props) => {

    const [formUser, setFormUser] = useState({
        ...props.update,
        show: false

    })

    const [progress, setProgress] = useState(0)
    const [updatePhoto, setUpdatePhoto] = useState(false)
    const [isProcessando, setisProcessando] = useState(false)


    useEffect(() => {

    }, [progress])

    const isUpdate = Object.keys(props.update).length > 0

    /**isUpdate ? updateUser(props.update._id, data) : createUser(data) */
    const typeReq = (data, config) => isUpdate ? updateUser(props.update._id, data, config) : createUser(data, config)

    const handleChange = (attr) => {
        const { value, name, checked } = attr.target
        const isCheck = name === 'show'



        if (name === 'photo') {
            setFormUser({
                ...formUser,
                'photo': attr.target.files[0]
            })
        } else {

            setFormUser({
                ...formUser,
                [name]: isCheck ? checked : value
            })
        }
        return;
    }

    const submitUser = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })
        // conversao dos dados paa formData
        let data = new FormData()

        Object.keys(formUser)
            .forEach(key => data.append(key, formUser[key]))

        if (typeof formUser.photo === "string") {
            //se photo é != de formdata é update (patch) que não trocou a foto. Tem que ser removido do formdata
            data.delete('photo')
        } 

        const config = {
            onUploadProgress: function (progressEvent) {
                let successPercent = Math.round(progressEvent.loaded * 100 / progressEvent.total)
                setProgress(successPercent)
            },
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }

        typeReq(data, config)
            .then((res) => {
                clearForm()
                message('success', `Usuário Cadastrado com sucesso.`)
                setisProcessando(false)

            })
            .catch((err) => {
                message('error', `Erro ao cadastrar usuário.${err.message}`)
                clearForm()
            })
        setisProcessando(false)
    }

    const clearForm = () => {
        setProgress(0)
        setUpdatePhoto(true)
        setFormUser({})
    }

    const isNotValid = () => {
        return Object.keys(formUser).some(k => typeof formUser[k] === "string" && formUser[k] === "") || (!formUser.photo)
    }

    const emailNotValid = (email) => {

        if (typeof (email) == 'undefined') {
            return false
        }
        if (email !== "") {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(email)) {
                return true
            } else {
                return false
            }
        }
    }

    const removePhoto = () => {
        setUpdatePhoto(true)
        setFormUser({
            ...formUser,
            photo: ""
        })
    }

    return (
        <>
            <ContCol fluid>
                {/* <Button size="sm" variant="success" onClick={() => cancelarUpdate()}> Limpar dados </Button> */}
                <Form.Group >
                    <Form.Control type="text" onChange={handleChange} name="name" value={formUser.name || ""}
                        placeholder="Nome do Usuário" />
                </Form.Group>

                <Form.Group >
                    <Form.Control type="text" onChange={handleChange} name="funcao" value={formUser.funcao || ""}
                        placeholder="Função do Usuário" />
                </Form.Group>

                <Form.Group >
                    <Form.Control isInvalid={emailNotValid(formUser.email)} type="email" onChange={handleChange} name="email" value={formUser.email || ""}
                        placeholder="Email do Usuário" />
                </Form.Group>

                <Form.Group >
                    <Form.Control type="password" onChange={handleChange} name="password" value={formUser.password || ""}
                        placeholder="Senha do Usuário" />
                </Form.Group>

                <Form.Group >
                    <Form.Control type="text" onChange={handleChange} name="briefing" value={formUser.briefing || ""}
                        as="textarea" rows={3} placeholder="Breve Descrição do usuário (exibição no portal)" />
                </Form.Group>

                <hr />

                <Form.Group >
                    {isUpdate && !updatePhoto ? (
                        <Pictur>
                            <img src={formUser.photo} alt={formUser.name} />
                            <span onClick={removePhoto}>Remover</span>
                        </Pictur>
                    ) : (
                            <>
                                {/*TODO: qundo vc chama o update este campo vem com a URL mas para envio ao cadastro este campo deve 
                    receber o buffer do file. Se submeter a atualização sem editar este campo dá erro. */}
                                <input name="photo" type="file" onChange={handleChange} />
                            </>
                        )}
                </Form.Group>
                <Form.Group className="d-flex">
                    <ButtonGroup toggle className="mr-3">
                        <ToggleButton
                            type="checkbox"
                            variant={Boolean(formUser.show) ? 'success' : 'danger'}
                            name="show"
                            onChange={handleChange}
                            checked={Boolean(formUser.show)}
                        >
                            {Boolean(formUser.show) ? <FaCheck /> : <FaTimes />} Exibir
                    </ToggleButton>
                    </ButtonGroup>


                </Form.Group>
                <Form.Group >
                    <Button variant="primary" disabled={isNotValid()} onClick={submitUser}>
                        {isProcessando ? <Spinner animation="grow" role="status" /> : ""}

                        {isUpdate ? "Atualizar" : "Cadastrar"}

                    </Button>
                </Form.Group>
                <br />
                {progress > 0 ? <ProgressBar striped variant="success" now={progress} /> : ""}
                <br />
            </ContCol>
        </>
    )

}

export default UsersForm



const Pictur = styled.div`

    display: flex;
    flex-direction: column;

    img{
        max-width: 200px;
        max-height: 200px;
    }

    span{
        cursor: pointer;
        color: #ccc;
        &:hover{
            color: red 
        }
    }


`

