import React, { useState, useEffect } from 'react'
import { Button, Spinner, Table } from 'react-bootstrap'
import styled from 'styled-components'
import Swal from 'sweetalert2'
import { getProducts, deleteProduct } from '../../services/admin'
import { FaRegEdit, FaTrashAlt, FaCheckCircle, FaTimesCircle } from 'react-icons/fa'
import { BsFillEyeFill } from 'react-icons/bs' 

const ListProducts = (props) => {
    const [products, setProducts] = useState([])

    const [isUpdate, setUpdate] = useState(false)

    useEffect(() => {
        setUpdate(false)
        let get = async () => {
            const prd = await getProducts();
            setProducts(prd.data)
        }
        if (!isUpdate) {
            get();
        }
        //clear
        return () => get = () => { };
    }, [isUpdate])


    const _detalhe = (prd) => {

        Swal.fire({
          
            title: prd.title,

            imageUrl: prd.photo,
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
            html: 
            `Categoria : ${prd.category.name} <br/> ` +
            `Descrição : ${prd.description} <br/> ` +
            `Descrição Completa : ${prd.complete_description} <br/> ` +
            `Preço R$: ${prd.price.toFixed(2)} <br/> ` +
            `Preço com desconto : ${prd.discount_price} <br/> ` +
            `Desconto % : ${prd.discount_price_percent} <br/> ` +
            `Id : ${prd._id} <br/> ` +
            `Link da Foto: <a href=${prd.photo}>links</a>  <br/>`,
        })
    }

    const _deleteCategory = async (obj) => {
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message || `Categoria excluída com sucesso.`,
            showConfirmButton: false,
            timer: 2500
        })

        Swal.fire({
            title: `Deseja excluir o produto ${obj.title} `,
            showCancelButton: true,
            confirmButtonText: `Sim`,
            cancelButtonText: `Não`,
        }).then((result) => {
            if (result.isConfirmed) {
                deleteProduct(obj._id)
                    .then(() => {
                        setUpdate(true)
                        message('success', `Produto ${obj.title} excluído com sucesso.`)
                    })
                    .catch(() => message('danger', `Erro ao excluir o produto`))
            }
        })
    }

    const sortCategoryByName = products.sort(function (a, b) {
        if (a.name > b.name) {
            return 1;
        }
        if (a.name < b.name) {
            return -1;
        }
        return 0;
    });

//view de tabela de produtos em admin
    return (
        <Scrolable>
            <NewTable striped hover size="sm">
                <thead>
                    <tr>
                        <THeadItem>Status</THeadItem>
                        <THeadItem>Destaque</THeadItem>
                        <THeadItem>Nome</THeadItem>
                        <THeadItem>Categoria</THeadItem>
                        <THeadItem>Preço</THeadItem>
                        <THeadItem>Desconto</THeadItem>
                        <THeadItem>Perc</THeadItem>
                        <THeadItem>Ações</THeadItem>
                    </tr>
                </thead>
                {sortCategoryByName.length <=0 ? <Spinner animation="grow" role="status"/>: ""}
                <tbody>
                    {sortCategoryByName.map((catg, i) => (
                        <tr key={i}>
                            <TbodyItem>{catg.status ? <FaCheckCirclegreen /> : <FaTimesCirclered />}</TbodyItem>
                            <TbodyItem>{catg.highlight ? <FaCheckCirclegreen /> : <FaTimesCirclered />}</TbodyItem>
                            <TbodyItem>{catg.title}</TbodyItem>
                            <TbodyItem>{catg.category.name}</TbodyItem>
                            <TbodyItem>{catg.price.toFixed(2)}</TbodyItem>
                            <TbodyItem>{catg.discount_price.toFixed(2)}</TbodyItem>
                            <TbodyItem>{catg.discount_price_percent}</TbodyItem>
                            <TbodyItem>

                                <div className="separe">




                                    <ActionButton onClick={() => _detalhe(catg)} variant="link" size="sm">
                                        <BsFillEyeFill />
                                    </ActionButton>

                                    <ActionButton onClick={() => props.updateProduct(catg)} variant="link" size="sm">
                                        <FaRegEdit />
                                    </ActionButton>


                                    <ActionButton onClick={() => _deleteCategory(catg)} variant="link" size="sm">
                                        <FaTrashAltred /> 
                                    </ActionButton>
                                </div>

                            </TbodyItem>
                        </tr>
                    ))}

                </tbody>
            </NewTable>
        </Scrolable>
    )
}

//importado por src\views\admin\products.js
export default ListProducts

const Scrolable = styled.div`                    
    min-width: 100%;
    height: auto;
    overflow-x: auto;
    overflow-y: auto;    
    padding: 20px;
`

const NewTable = styled(Table)`
    font-size: 14px
`

const THeadItem = styled.th`
    background: #666;
    color:#eee;
    text-align:center;
`
const TbodyItem = styled.td`
text-align:center;

    :nth-child(1){  width: 10%; }
    :nth-child(2){  width: 10%; }
    :nth-child(3){  width: 30%; }
    :nth-child(4){  width: 10%; }
    :nth-child(5){  width: 10%; }
    :nth-child(6){  width: 10%; }
    :nth-child(7){  width: 10%; }
    :nth-child(8){  
        width: 10%;
        .separe{
        
        display: flex;
        justify-content :space-around;
        }
    } 
`
const ActionButton = styled(Button)`
    padding:2px 4px;
    font-weight:500;
    font-size: 16px;

    :hover {
        opacity:0.4
    }
`

const FaTrashAltred = styled(FaTrashAlt)`
color:red;
`

const FaCheckCirclegreen = styled(FaCheckCircle)`
color:green;
`

const FaTimesCirclered = styled(FaTimesCircle)`
color:red;
`

/* <FaCheckCircle /> : <FaTimesCircle */

// {
//     "highlight": true
//     "status": true
//     "_id": "5f7eea912ce7e17121ef72e0"
//     "category": "5f730ecab8f6c96fac7f68fd"
//     "description": "pizza com tudo dentro"
//     "complete_description": "não sei todos os ingredientes"
//     "price": 30
//     "discount_price": 27
//     "discount_price_percent": 10
//     "title": "Portuguesa"
//     "photo": "https://pizza-infnet.s3.amazonaws.com/product/taycabral..jpg"
//     "last_modification_date": "2020-10-08T10:31:45.828Z"
//     "__v": 0
//     "last_modified_by": "5f74650a3bad77eb5a4cf41e"
// }
