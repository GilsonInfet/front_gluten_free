
import styled from "styled-components";
import Swal from "sweetalert2";

/**
 *type danger /info / sucess 
 *message mensagem da desejada
 */
export const message = (type, message) => {
    Swal.fire({
        position: 'center',
        icon: type,
        title: message,
        showConfirmButton: false,
        timer: 2500
    })
}

/** 
 * Uso:
 *  swalConfirmation(`TUA MENSAGEM`, 
 * ()=>{FUNÇÃO A SER EXECUTADA EM CASO DE SIM  })}  
 *  */
export const swalConfirmation = async (mensagem, callback) => {

    const resultConfirmation = await Swal.fire({
        title: `${mensagem}`,
        showCancelButton: true,
        confirmButtonText: `Sim`,
        cancelButtonText: `Não`
    })
    
    if (resultConfirmation.isConfirmed) {
        callback()
    }
    return

}


export const Scrolable = styled.div`                    
    min-width: 100%;
    height: auto;
    overflow-x: auto;
    overflow-y: scroll;    
    padding: 20px;
`