import React from 'react'
import {
    FacebookIcon, FacebookShareButton, LinkedinIcon, LinkedinShareButton,
    TwitterIcon,
    TwitterShareButton, WhatsappIcon, WhatsappShareButton, WorkplaceIcon, WorkplaceShareButton
} from "react-share"

/**requeres props quote, hashtag, shareUrl */
export function Sharegroup(props) {
  

    const  shareUrl = window.location.href 
    const  quote = props.quote || "Confira este site."
    const  hashtag = props.hashtag || "#GLUTENFREE"
    const  size = props.size || 32
    const round = props.round || true    
    const style = {
        bl:{display :"block",
            height: "180px",
            width:"30px",
            position: "fixed", 
            left: 0, 
            top: "25vh",
            zIndex: "99",
            }
    }
    
    
  
    
    return (
        <div style={style.bl}>
            <FacebookShareButton quote={quote} hashtag={hashtag} url={shareUrl}>
                <FacebookIcon size={size} round={round} />
            </FacebookShareButton>

            <WorkplaceShareButton quote={quote} hashtag={hashtag} url={shareUrl}>
                <WorkplaceIcon size={size} round={round} />
            </WorkplaceShareButton>

            <LinkedinShareButton  quote={quote} hashtag={hashtag} url={shareUrl}>
                <LinkedinIcon size={size} round={round} />
            </LinkedinShareButton>

            <TwitterShareButton quote={quote} hashtag={hashtag} url={shareUrl}>
                <TwitterIcon size={size} round={round} />
            </TwitterShareButton>

            <WhatsappShareButton quote={quote} hashtag={hashtag} url={shareUrl}>
                <WhatsappIcon size={size} round={round} />
            </WhatsappShareButton>
 
            </div>
    )

   
}

