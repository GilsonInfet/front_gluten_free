import React, { useEffect } from 'react';
import Helmet from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { actionGuardaContent } from '../store/content/contentsaver';

const TitleComponent = ({ title }) => {

    const businessTitle = useSelector(state => state.content.business.title)
    const dispatch = useDispatch()
    useEffect(() => {      
        dispatch(actionGuardaContent())
}, [dispatch]) 

    return (
        <Helmet>
            <title>{title ? `${businessTitle} - ${title}` : businessTitle}</title>
        </Helmet>
    );
};

export { TitleComponent };