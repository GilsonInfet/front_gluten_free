import React from 'react'
import { Col, Container} from "react-bootstrap";

export const ContCol = ({children, fluid}) => {
    return(
        <Container fluid={fluid}>
            {/* <Col xs={12} sm={12} md={5} lg={6} xl={6} >     */}
            <Col xs={12} sm={12} md={12} lg={8} xl={6}>
                {children}    
            </Col>
        </Container>
        )
}
    