import React, { useState, useEffect } from 'react'
import { Button, ButtonGroup, Form, ProgressBar, Spinner, ToggleButton } from 'react-bootstrap'
import Swal from 'sweetalert2/dist/sweetalert2.js'
// import {  updateProduct } from '../../services/admin'

import styled from 'styled-components'
import { updateProduct } from '../services/admin'






const InputFileComponent = (props) => {

    const [progress, setProgress] = useState(20)
    const [isProcessando, setisProcessando] = useState(false)

const chandleChange = (attr) => {
    props.setObjeto({
        ...props.objeto,
        'photo': attr.target.files[0]
    })
}


const handleChange = (attr) => {
    const { value, name, checked } = attr.target
    const isCheck = name === 'status' || name === 'highlight'



    if (name === 'photo') {
        setFormProduct({
            ...formProduct,
            'photo': attr.target.files[0]
        })
    } else {

        setFormProduct({
            ...formProduct,
            [name]: isCheck ? checked : value
        })
    }
    return;
}

    const submitProduct = async () => {

        setisProcessando(true)
        const message = (type, message) => Swal.fire({
            position: 'center',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })
        // conversao dos dados paa formData
        let data = new FormData()

        const config = {
            onUploadProgress: function (progressEvent) {
                let successPercent = Math.round(progressEvent.loaded * 100 / progressEvent.total)
                setProgress(successPercent)
            },
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }

        updateProduct(props.update._id, data)
            .then((res) => {

                message('success', `Produto Cadastrado com sucesso.`)
                setisProcessando(false)
            })
            .catch((err) => message('error', `Erro ao cadastrar produto.`))
            setisProcessando(false)
    }


  return (
    <>
    <hr/>
        <br/><br/>
        <input name="photo" type="file" onChange={handleChange} /> 
        <br/><br/> 
        {progress > 0 ? <ProgressBar striped variant="success" now={progress} /> : ""}
        <br/>
    </>
  )
}

export default InputFileComponent
