import React from 'react'
import { Route, Router, Switch, Redirect } from 'react-router-dom'
import history from './config/history'

import LayoutAdmin from './components/Layout/admin';
import LayoutPortal from './components/Layout/portal';
//auth
import Login from './views/auth/login';

//admin
import ProductsAdmin from './views/admin/products'
import CategoriesAdmin from './views/admin/categories'
// import Swall from './views/admin/testeswall'

// import Formtest from '../src/components/category/formtest'

//portal
import Home from './views/portal/home/index'
import About from './views/portal/about'
import Product from './views/portal/products/index.js'
import Services from './views/portal/service'
import Contato from './views/portal/contact'

import { isAuthenticated } from './config/auth';
import ContentAbout from './components/content/content_about';
import ContentService from './components/content/content_service';
import Equipe from './views/admin/usersView';
import InfoView from './views/admin/infoView';
import BannerView from './views/admin/bannerView';
import HomePortalView from './views/admin/homePortalView';


const AdminRoute = ({ ...rest }) => {
    if (!isAuthenticated()) {
       
        return <Redirect to='/login' />
    }
    return <Route {...rest} />
}


/**O PROFESSOR TINHA MONTADO DUAS VIEWS, UMA PARA O PORTAL E PARA O ADMIN, COMO FOI DE DIFÍCIL ENTENDIMENTO 
 * IMPORTOU OS COMPONENTES DIRETAMENTE PRA CÁ AO INVÉS DAS VIEWS
 * ENTÃO PELO FORMATO ATUAL, QUE EU QUISER EXIBIR UMA TELA E ROTA NOVA, DEVO IMPORTÁ-LAS PRA CÁ.
 */
const Routers = () => (
    <Router history={history}>
        <Switch>
            <Route exact component={Login} path="/login" />

            <Route path="/admin">
                <LayoutAdmin>
                    <AdminRoute exact path="/admin" component={() => <h5>Home de admin. Use o menu à esquerda para acessar as opções de cadastro do portal. </h5>} />
                    <AdminRoute exact path="/admin/categorias" component={CategoriesAdmin} />
                    <AdminRoute exact path="/admin/produtos" component={ProductsAdmin} />
                    <AdminRoute exact path="/admin/content-about" component={ContentAbout} />
                    <AdminRoute exact path="/admin/content-service" component={ContentService} />
                    <AdminRoute exact path="/admin/equipe" component={Equipe} />
                    <AdminRoute exact path="/admin/info" component={InfoView} />
                    <AdminRoute exact path="/admin/banner" component={BannerView} />
                    <AdminRoute exact path="/admin/homeconfig" component={HomePortalView} />
                    
                </LayoutAdmin>
            </Route>

            <Route path="/">
                <LayoutPortal>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/sobre" component={About} />
                    <Route exact path="/produtos" component={Product} />
                    <Route exact path="/servicos" component={Services} />
                    <Route exact path="/contato" component={Contato} />
                    
                </LayoutPortal>
            </Route>

        </Switch>
    </Router>
)

export default Routers;
